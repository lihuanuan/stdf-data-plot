#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : var_language.py
@Author  : Link
@Time    : 2022/12/24 11:22
@Mark    :

    datas=[
        ('parser_core\\dll_parser\\stdf_ctype.dll', '.'),
        ('chart_core\\chart_pyqtgraph\\core\\visual\\visual.cp37-win_amd64.pyd', '.'),
        ('colors\\CET-C6.csv', '.\\colors'),
        ('colors\\CET-D8.csv', '.\\colors'),
        ('chart_core\\jmp_app', '\\chart_core'),
    ],
"""
from environs import Env

env = Env()
env.read_env()


class BaseConfig:
    SERVER_HOST: str = env.str("SERVER_HOST", default="http://127.0.0.1:5000")
    NGINX_HOST: str = env.str("NGINX_HOST", default="http://127.0.0.1:8000")
    TCP_PORT: int = env.int("TCP_PORT", default=11311)
    UNZIP_BACKEND: str = env.str("UNZIP_BACKEND", default="C:\\\"Program Files\"\\7-Zip\\7z.exe")  # 解压缩后端
    SKIP_FTR: bool = env.bool("SKIP_FTR", default=True)
    DISK: str = env.str("DISK", default="D")
    ROOT_PATH: str = env.str("DISK", default=r'D:\\')  # 用于文件树
    FILE_FILTER: str = env.str("FILE_FILTER", default="*.std;*.stdf;*.std_temp")
    EN: bool = env.bool("EN", default=True)  # 是否英文
    HTML_REPORT_MAX_ROWS: int = env.int("HTML_REPORT_MAX_ROWS", default=10)
    HAVE_LICENSE: bool = False


class LanguageZh:
    if BaseConfig.EN:
        Message = ""
        LicenseMessage = ""
        DebugMessage = "If the file parsing fails, the STDF file can be fed back"
        GraphSetting = {
            "GraphSetting": "GraphSetting",
            "GraphPlotBin": "GraphPlotBin",
            "GraphBins": "GraphBins",
            "GraphScreen": "GraphScreen",
            "GraphMeanAddSubSigma": "GraphMeanAddSubSigma",
            "GraphPlotColumn": "GraphPlotColumn",
            "GraphPlotScatterSimple": "GraphPlotScatterSimple",
            "GraphPlotScatterSimpleNum": "GraphPlotScatterSimpleNum",
            "GraphPlotFloatRound": "GraphPlotFloatRound",
            "GraphPlotWidth": "GraphPlotWidth",
            "GraphPlotHeight": "GraphPlotHeight",

            "GraphCpkLoClamp": "GraphCpkLoClamp",
            "GraphCpkHiClamp": "GraphCpkHiClamp",
            "GraphTopFailClamp": "GraphTopFailClamp",
            "GraphRejectClamp": "GraphRejectClamp",
            # "GraphOnlyBinNM": "GraphOnlyBinNM",
            "PandasScreen": "PandasScreen",
            "PandasLogDefaultRow": "PandasLogDefaultRow",
            "HtmlReportMaxRows": "HtmlReportMaxRows",
        }
    else:
        Message = ""
        LicenseMessage = ""
        DebugMessage = "文件解析失败, 可将STDF文件进行反馈"
        GraphSetting = {
            "GraphSetting": "PyqtGraph绘图相关设定",
            "GraphPlotBin": "自定义Graph分布组数",
            "GraphBins": "Graph分布组数量",
            "GraphScreen": "Graph绘图值筛选区间",
            "GraphMeanAddSubSigma": "Average_Sigma±区间",
            "GraphPlotColumn": "绘图分列数",
            "GraphPlotScatterSimple": "开启散点图抽样",
            "GraphPlotScatterSimpleNum": "散点趋势图抽样数",
            "GraphPlotFloatRound": "绘图小数精确位",
            "GraphPlotWidth": "绘图最大宽度",
            "GraphPlotHeight": "绘图最大高度",

            "GraphCpkLoClamp": "CPK最小值卡控",
            "GraphCpkHiClamp": "CPK最大值卡控",
            "GraphTopFailClamp": "TopFail颗数最小值卡控",
            "GraphRejectClamp": "失效颗数最小值卡控",
            # "GraphOnlyBinNM": "只显示BIN编号不显示BIN名字",
            "PandasScreen": "使用所有的数据计算均值",
            "PandasLogDefaultRow": "Log最大显示行数",
            "HtmlReportMaxRows": "HtmlReport最大显示的芯片数据量",
        }

    Altair = {

    }

    R = {

    }

    PLOT_BACKEND: tuple = ("PyqtGraph",)  # , "JMP", "Altair", "R")

    kwargs = {}
    for key, values in GraphSetting.items():
        kwargs[values] = key
    for key, values in Altair.items():
        kwargs[values] = key


language = LanguageZh
