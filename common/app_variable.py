"""
-*- coding: utf-8 -*-
@Author  : Link
@Time    : 2021/12/16 9:21
@Software: PyCharm
@File    : app_variable.py
@Remark  : 
"""
import os
from dataclasses import dataclass
from typing import Union, Dict

import pandas as pd

# 尽可能将数据类型都定义为 dataclass 或是 pydantic ValidationError, BaseModel
from var_language import BaseConfig
from common.data_class_interface.for_analysis_stdf import PtmdModule, PRR_HEAD, DTP_HEAD, PTMD_HEAD, BIN_HEAD, \
    STDF_HEAD, DTR_HEAD
from common.data_class_interface.for_calculation import Calculation


@dataclass
class DataLiBackup:
    """
    用于缓存修改前的数据, 暂时不需要
    """
    select_summary: pd.DataFrame = None
    prr_df: pd.DataFrame = None


@dataclass
class ToChartCsv:
    # TODO: Must
    df: pd.DataFrame = None  # 所有数据
    temp_df: pd.DataFrame = None  # 所有数据
    group_df: Dict[str, pd.DataFrame] = None
    chart_df: pd.DataFrame = None  # 前台展示数据, 基于分组后的select
    group_chart_df: Dict[str, pd.DataFrame] = None
    select_group: set = None

    # TODO: Optional PAT
    limit: pd.DataFrame = None
    group_limit: Dict[str, pd.DataFrame] = None


@dataclass
class DataModule:
    """
    数据空间整合后的数据模型
    """
    prr_df: pd.DataFrame = None
    dtp_df: pd.DataFrame = None  # 数据
    ptmd_df: pd.DataFrame = None  # 测试项目相关
    bin_df: Union[pd.DataFrame, None] = None
    ptmd_df_limit: Union[pd.DataFrame, None] = None
    log_df: Union[pd.DataFrame, None] = None


class DatatType:
    FTR: str = "FTR"
    PTR: str = "PTR"
    MPR: str = "MPR"


class FailFlag:
    PASS = 1
    FAIL = 0


class LimitType:
    NoHighLimit = "NA"
    EqualHighLimit = "LE"
    ThenHighLimit = "LT"
    HiTypes = [EqualHighLimit, ThenHighLimit, NoHighLimit, ]

    NoLowLimit = "NA"
    EqualLowLimit = "GE"
    ThenLowLimit = "GT"
    LoTypes = [EqualLowLimit, ThenLowLimit, NoLowLimit, ]


class PartFlags:
    ALL = 0
    FIRST = 1
    RETEST = 2
    FINALLY = 3
    FIRST_XY = 4
    XY_COORD = 5
    PART_FLAGS = ('ALL', 'FIRST', 'RETEST', 'FINALLY', "FIRST_XY", "XY_COORD")


class ReadFail:
    Y = 1
    N = 0


class DownType:
    Wait = False
    Success = True
    Stop = None


class GlobalVariable:
    """
    用来放一些全局变量
    TODO: 以大写作为主要的HEAD
    """
    LICENSE_CONTROL = False  # 用来控制奇奇怪怪的功能
    LICENSE_MESSAGE = ""
    DEBUG = True
    SAVE_PKL = False  # 用来将数据保存到二进制数据中用来做APP测试 TODO: 此版本暂时作废
    HOME_PATH = r"{}:\1_STDF".format(BaseConfig.DISK)
    SQLITE_PATH = HOME_PATH + r"\stdf_info.db"  # 用于存summary
    CACHE_PATH = HOME_PATH + r"\STDF_CACHE"
    JMP_CACHE_PATH = HOME_PATH + r"\JMP_CACHE"
    LIMIT_PATH = HOME_PATH + r"\LIMIT_CACHE"
    NGINX_PATH = HOME_PATH + r"\NGINX_CACHE"
    SWAP_PATH = HOME_PATH + r"\SWAP_PATH"

    CET_C6_COLOR = ['0.967,0.214,0.103\n', '0.967,0.220,0.092\n', '0.966,0.228,0.082\n', '0.967,0.239,0.072\n',
                    '0.967,0.252,0.063\n', '0.968,0.266,0.055\n', '0.969,0.282,0.047\n', '0.971,0.298,0.040\n',
                    '0.972,0.315,0.033\n', '0.974,0.333,0.027\n', '0.976,0.350,0.023\n', '0.978,0.368,0.019\n',
                    '0.980,0.386,0.015\n', '0.983,0.403,0.012\n', '0.985,0.421,0.010\n', '0.987,0.438,0.007\n',
                    '0.989,0.455,0.005\n', '0.991,0.472,0.003\n', '0.992,0.489,0.001\n', '0.994,0.505,0.000\n',
                    '0.996,0.522,0.000\n', '0.998,0.538,0.000\n', '0.999,0.554,0.000\n', '1.000,0.569,0.000\n',
                    '1.000,0.585,0.000\n', '1.000,0.600,0.000\n', '1.000,0.616,0.000\n', '1.000,0.631,0.000\n',
                    '1.000,0.646,0.000\n', '1.000,0.660,0.000\n', '1.000,0.675,0.000\n', '1.000,0.689,0.000\n',
                    '1.000,0.703,0.000\n', '1.000,0.717,0.000\n', '1.000,0.730,0.000\n', '1.000,0.743,0.000\n',
                    '0.997,0.755,0.000\n', '0.993,0.766,0.000\n', '0.988,0.777,0.000\n', '0.983,0.787,0.000\n',
                    '0.976,0.795,0.000\n', '0.968,0.803,0.000\n', '0.959,0.809,0.000\n', '0.949,0.814,0.000\n',
                    '0.938,0.817,0.000\n', '0.925,0.820,0.000\n', '0.912,0.821,0.000\n', '0.898,0.821,0.000\n',
                    '0.883,0.820,0.000\n', '0.868,0.817,0.000\n', '0.852,0.814,0.000\n', '0.835,0.810,0.000\n',
                    '0.818,0.805,0.000\n', '0.801,0.800,0.000\n', '0.783,0.794,0.000\n', '0.765,0.788,0.000\n',
                    '0.747,0.781,0.000\n', '0.729,0.774,0.000\n', '0.711,0.767,0.000\n', '0.692,0.760,0.000\n',
                    '0.674,0.752,0.000\n', '0.655,0.745,0.000\n', '0.637,0.737,0.000\n', '0.618,0.730,0.000\n',
                    '0.600,0.722,0.000\n', '0.581,0.714,0.000\n', '0.562,0.707,0.001\n', '0.543,0.699,0.003\n',
                    '0.524,0.691,0.004\n', '0.505,0.684,0.006\n', '0.486,0.676,0.008\n', '0.467,0.668,0.010\n',
                    '0.448,0.661,0.014\n', '0.429,0.654,0.017\n', '0.409,0.647,0.022\n', '0.390,0.640,0.027\n',
                    '0.371,0.633,0.034\n', '0.352,0.627,0.042\n', '0.334,0.621,0.051\n', '0.316,0.616,0.061\n',
                    '0.298,0.611,0.071\n', '0.281,0.607,0.082\n', '0.265,0.604,0.094\n', '0.249,0.602,0.106\n',
                    '0.235,0.601,0.119\n', '0.222,0.601,0.133\n', '0.210,0.601,0.147\n', '0.199,0.603,0.162\n',
                    '0.190,0.606,0.177\n', '0.183,0.610,0.193\n', '0.177,0.614,0.210\n', '0.173,0.620,0.227\n',
                    '0.170,0.626,0.244\n', '0.169,0.632,0.262\n', '0.169,0.640,0.281\n', '0.169,0.648,0.299\n',
                    '0.171,0.656,0.318\n', '0.173,0.665,0.337\n', '0.176,0.674,0.356\n', '0.179,0.683,0.375\n',
                    '0.181,0.692,0.395\n', '0.184,0.702,0.414\n', '0.187,0.711,0.434\n', '0.190,0.721,0.454\n',
                    '0.192,0.731,0.473\n', '0.194,0.741,0.493\n', '0.195,0.751,0.513\n', '0.196,0.760,0.533\n',
                    '0.197,0.770,0.552\n', '0.198,0.780,0.572\n', '0.197,0.790,0.592\n', '0.197,0.800,0.612\n',
                    '0.196,0.810,0.632\n', '0.194,0.820,0.652\n', '0.192,0.830,0.672\n', '0.190,0.839,0.692\n',
                    '0.187,0.848,0.712\n', '0.183,0.858,0.732\n', '0.180,0.866,0.752\n', '0.176,0.874,0.771\n',
                    '0.172,0.882,0.790\n', '0.167,0.889,0.809\n', '0.163,0.895,0.827\n', '0.159,0.901,0.845\n',
                    '0.155,0.905,0.862\n', '0.151,0.908,0.878\n', '0.149,0.910,0.893\n', '0.147,0.911,0.907\n',
                    '0.146,0.911,0.920\n', '0.146,0.909,0.932\n', '0.147,0.906,0.943\n', '0.149,0.901,0.953\n',
                    '0.152,0.896,0.961\n', '0.156,0.889,0.969\n', '0.160,0.881,0.975\n', '0.164,0.873,0.980\n',
                    '0.168,0.863,0.985\n', '0.172,0.853,0.989\n', '0.176,0.843,0.992\n', '0.180,0.832,0.995\n',
                    '0.183,0.820,0.997\n', '0.186,0.809,0.999\n', '0.188,0.797,1.000\n', '0.190,0.785,1.000\n',
                    '0.192,0.773,1.000\n', '0.193,0.760,1.000\n', '0.193,0.748,1.000\n', '0.193,0.736,1.000\n',
                    '0.192,0.723,1.000\n', '0.191,0.711,1.000\n', '0.189,0.699,1.000\n', '0.187,0.687,1.000\n',
                    '0.184,0.674,1.000\n', '0.181,0.662,1.000\n', '0.177,0.650,1.000\n', '0.173,0.638,1.000\n',
                    '0.169,0.626,1.000\n', '0.165,0.614,1.000\n', '0.161,0.603,1.000\n', '0.158,0.591,1.000\n',
                    '0.156,0.580,1.000\n', '0.155,0.570,1.000\n', '0.155,0.559,1.000\n', '0.158,0.550,1.000\n',
                    '0.163,0.541,1.000\n', '0.170,0.533,1.000\n', '0.180,0.525,1.000\n', '0.193,0.519,1.000\n',
                    '0.207,0.513,1.000\n', '0.224,0.509,1.000\n', '0.242,0.506,1.000\n', '0.262,0.504,1.000\n',
                    '0.283,0.503,1.000\n', '0.304,0.503,1.000\n', '0.326,0.504,1.000\n', '0.348,0.507,1.000\n',
                    '0.370,0.510,1.000\n', '0.392,0.514,1.000\n', '0.414,0.519,1.000\n', '0.435,0.525,1.000\n',
                    '0.457,0.531,1.000\n', '0.478,0.538,1.000\n', '0.498,0.546,1.000\n', '0.518,0.553,1.000\n',
                    '0.538,0.561,1.000\n', '0.558,0.570,1.000\n', '0.577,0.578,1.000\n', '0.595,0.586,1.000\n',
                    '0.614,0.595,1.000\n', '0.632,0.604,1.000\n', '0.649,0.613,1.000\n', '0.667,0.622,1.000\n',
                    '0.684,0.631,1.000\n', '0.701,0.640,1.000\n', '0.718,0.648,1.000\n', '0.734,0.657,1.000\n',
                    '0.751,0.666,1.000\n', '0.767,0.675,1.000\n', '0.783,0.684,1.000\n', '0.799,0.692,1.000\n',
                    '0.814,0.701,1.000\n', '0.830,0.709,1.000\n', '0.845,0.716,1.000\n', '0.861,0.723,1.000\n',
                    '0.876,0.730,1.000\n', '0.890,0.736,1.000\n', '0.905,0.740,0.994\n', '0.919,0.744,0.987\n',
                    '0.932,0.747,0.979\n', '0.945,0.749,0.970\n', '0.957,0.750,0.959\n', '0.969,0.749,0.947\n',
                    '0.980,0.747,0.934\n', '0.990,0.743,0.920\n', '0.999,0.738,0.904\n', '1.000,0.732,0.887\n',
                    '1.000,0.725,0.870\n', '1.000,0.716,0.851\n', '1.000,0.707,0.832\n', '1.000,0.696,0.812\n',
                    '1.000,0.685,0.792\n', '1.000,0.673,0.771\n', '1.000,0.661,0.750\n', '1.000,0.648,0.729\n',
                    '1.000,0.635,0.707\n', '1.000,0.621,0.686\n', '1.000,0.607,0.664\n', '1.000,0.593,0.643\n',
                    '1.000,0.579,0.621\n', '1.000,0.564,0.600\n', '1.000,0.549,0.578\n', '1.000,0.535,0.557\n',
                    '1.000,0.520,0.536\n', '1.000,0.505,0.515\n', '1.000,0.489,0.494\n', '1.000,0.474,0.473\n',
                    '1.000,0.458,0.452\n', '1.000,0.443,0.431\n', '1.000,0.427,0.411\n', '1.000,0.411,0.390\n',
                    '1.000,0.395,0.370\n', '1.000,0.378,0.350\n', '1.000,0.362,0.330\n', '1.000,0.346,0.310\n',
                    '1.000,0.329,0.291\n', '0.999,0.313,0.272\n', '0.995,0.297,0.254\n', '0.991,0.282,0.235\n',
                    '0.987,0.267,0.218\n', '0.984,0.253,0.201\n', '0.980,0.240,0.185\n', '0.977,0.230,0.169\n',
                    '0.974,0.221,0.154\n', '0.972,0.215,0.140\n', '0.970,0.212,0.127\n', '0.968,0.211,0.115\n']
    CET_D8_COLOR = ['0.000,0.165,0.844\n', '0.000,0.167,0.840\n', '0.000,0.169,0.837\n', '0.000,0.171,0.833\n',
                    '0.000,0.173,0.829\n', '0.000,0.175,0.826\n', '0.000,0.177,0.822\n', '0.000,0.179,0.819\n',
                    '0.026,0.181,0.815\n', '0.053,0.183,0.811\n', '0.073,0.185,0.808\n', '0.090,0.187,0.804\n',
                    '0.104,0.189,0.801\n', '0.116,0.191,0.797\n', '0.128,0.193,0.794\n', '0.138,0.195,0.790\n',
                    '0.147,0.197,0.786\n', '0.156,0.199,0.783\n', '0.164,0.201,0.779\n', '0.172,0.203,0.776\n',
                    '0.179,0.205,0.772\n', '0.186,0.207,0.768\n', '0.192,0.208,0.765\n', '0.199,0.210,0.761\n',
                    '0.205,0.212,0.758\n', '0.210,0.214,0.754\n', '0.216,0.216,0.751\n', '0.221,0.218,0.747\n',
                    '0.226,0.220,0.744\n', '0.231,0.222,0.740\n', '0.236,0.223,0.736\n', '0.240,0.225,0.733\n',
                    '0.245,0.227,0.729\n', '0.249,0.229,0.726\n', '0.253,0.231,0.722\n', '0.257,0.233,0.719\n',
                    '0.261,0.235,0.715\n', '0.265,0.236,0.712\n', '0.269,0.238,0.708\n', '0.272,0.240,0.705\n',
                    '0.276,0.242,0.701\n', '0.279,0.244,0.697\n', '0.283,0.246,0.694\n', '0.286,0.247,0.690\n',
                    '0.289,0.249,0.687\n', '0.292,0.251,0.683\n', '0.295,0.253,0.680\n', '0.298,0.255,0.676\n',
                    '0.301,0.256,0.673\n', '0.304,0.258,0.669\n', '0.306,0.260,0.666\n', '0.309,0.262,0.662\n',
                    '0.311,0.264,0.659\n', '0.314,0.265,0.655\n', '0.316,0.267,0.652\n', '0.319,0.269,0.648\n',
                    '0.321,0.271,0.645\n', '0.323,0.273,0.641\n', '0.326,0.274,0.638\n', '0.328,0.276,0.634\n',
                    '0.330,0.278,0.631\n', '0.332,0.280,0.627\n', '0.334,0.282,0.623\n', '0.336,0.283,0.620\n',
                    '0.338,0.285,0.616\n', '0.340,0.287,0.613\n', '0.342,0.289,0.609\n', '0.343,0.290,0.606\n',
                    '0.345,0.292,0.602\n', '0.347,0.294,0.599\n', '0.349,0.296,0.595\n', '0.350,0.297,0.592\n',
                    '0.352,0.299,0.588\n', '0.353,0.301,0.585\n', '0.355,0.303,0.581\n', '0.356,0.304,0.578\n',
                    '0.358,0.306,0.574\n', '0.359,0.308,0.571\n', '0.361,0.310,0.567\n', '0.362,0.311,0.564\n',
                    '0.363,0.313,0.560\n', '0.365,0.315,0.557\n', '0.366,0.316,0.553\n', '0.367,0.318,0.550\n',
                    '0.368,0.320,0.546\n', '0.369,0.322,0.543\n', '0.371,0.323,0.539\n', '0.372,0.325,0.536\n',
                    '0.373,0.327,0.532\n', '0.374,0.329,0.529\n', '0.375,0.330,0.525\n', '0.376,0.332,0.522\n',
                    '0.377,0.334,0.518\n', '0.378,0.335,0.515\n', '0.378,0.337,0.511\n', '0.379,0.339,0.508\n',
                    '0.380,0.340,0.505\n', '0.381,0.342,0.501\n', '0.382,0.344,0.498\n', '0.382,0.346,0.494\n',
                    '0.383,0.347,0.490\n', '0.384,0.349,0.487\n', '0.384,0.351,0.483\n', '0.385,0.352,0.480\n',
                    '0.386,0.354,0.476\n', '0.386,0.356,0.473\n', '0.387,0.357,0.469\n', '0.387,0.359,0.466\n',
                    '0.388,0.361,0.462\n', '0.388,0.362,0.459\n', '0.389,0.364,0.455\n', '0.389,0.366,0.452\n',
                    '0.390,0.368,0.448\n', '0.390,0.369,0.445\n', '0.390,0.371,0.441\n', '0.391,0.373,0.438\n',
                    '0.391,0.374,0.434\n', '0.391,0.376,0.431\n', '0.392,0.378,0.427\n', '0.392,0.379,0.424\n',
                    '0.392,0.381,0.420\n', '0.392,0.383,0.417\n', '0.393,0.384,0.413\n', '0.393,0.386,0.409\n',
                    '0.393,0.388,0.406\n', '0.393,0.389,0.402\n', '0.393,0.391,0.399\n', '0.393,0.393,0.395\n',
                    '0.397,0.393,0.392\n', '0.403,0.393,0.390\n', '0.410,0.392,0.387\n', '0.417,0.392,0.385\n',
                    '0.423,0.391,0.382\n', '0.429,0.391,0.380\n', '0.436,0.390,0.377\n', '0.442,0.390,0.375\n',
                    '0.448,0.389,0.373\n', '0.454,0.388,0.370\n', '0.460,0.388,0.368\n', '0.466,0.387,0.365\n',
                    '0.472,0.387,0.363\n', '0.478,0.386,0.360\n', '0.484,0.385,0.358\n', '0.490,0.385,0.355\n',
                    '0.496,0.384,0.353\n', '0.501,0.383,0.350\n', '0.507,0.383,0.348\n', '0.513,0.382,0.346\n',
                    '0.518,0.381,0.343\n', '0.524,0.380,0.341\n', '0.529,0.380,0.338\n', '0.535,0.379,0.336\n',
                    '0.540,0.378,0.333\n', '0.545,0.377,0.331\n', '0.551,0.376,0.328\n', '0.556,0.376,0.326\n',
                    '0.561,0.375,0.323\n', '0.567,0.374,0.321\n', '0.572,0.373,0.318\n', '0.577,0.372,0.316\n',
                    '0.582,0.371,0.313\n', '0.587,0.370,0.311\n', '0.593,0.369,0.308\n', '0.598,0.368,0.306\n',
                    '0.603,0.367,0.303\n', '0.608,0.366,0.301\n', '0.613,0.365,0.298\n', '0.618,0.364,0.296\n',
                    '0.623,0.363,0.293\n', '0.628,0.362,0.291\n', '0.633,0.361,0.288\n', '0.638,0.360,0.286\n',
                    '0.643,0.359,0.283\n', '0.648,0.358,0.281\n', '0.653,0.356,0.278\n', '0.657,0.355,0.275\n',
                    '0.662,0.354,0.273\n', '0.667,0.353,0.270\n', '0.672,0.351,0.268\n', '0.677,0.350,0.265\n',
                    '0.682,0.349,0.263\n', '0.686,0.347,0.260\n', '0.691,0.346,0.257\n', '0.696,0.345,0.255\n',
                    '0.701,0.343,0.252\n', '0.705,0.342,0.250\n', '0.710,0.340,0.247\n', '0.715,0.339,0.244\n',
                    '0.720,0.337,0.242\n', '0.724,0.336,0.239\n', '0.729,0.334,0.237\n', '0.734,0.333,0.234\n',
                    '0.738,0.331,0.231\n', '0.743,0.330,0.229\n', '0.748,0.328,0.226\n', '0.752,0.326,0.223\n',
                    '0.757,0.324,0.221\n', '0.761,0.323,0.218\n', '0.766,0.321,0.215\n', '0.771,0.319,0.212\n',
                    '0.775,0.317,0.210\n', '0.780,0.315,0.207\n', '0.784,0.313,0.204\n', '0.789,0.311,0.201\n',
                    '0.794,0.309,0.199\n', '0.798,0.307,0.196\n', '0.803,0.305,0.193\n', '0.807,0.303,0.190\n',
                    '0.812,0.301,0.187\n', '0.816,0.299,0.184\n', '0.821,0.296,0.182\n', '0.825,0.294,0.179\n',
                    '0.830,0.292,0.176\n', '0.834,0.290,0.173\n', '0.839,0.287,0.170\n', '0.843,0.285,0.167\n',
                    '0.848,0.282,0.164\n', '0.852,0.280,0.161\n', '0.857,0.277,0.158\n', '0.861,0.274,0.155\n',
                    '0.866,0.271,0.152\n', '0.870,0.269,0.149\n', '0.875,0.266,0.145\n', '0.879,0.263,0.142\n',
                    '0.884,0.260,0.139\n', '0.888,0.257,0.136\n', '0.892,0.254,0.132\n', '0.897,0.250,0.129\n',
                    '0.901,0.247,0.126\n', '0.906,0.244,0.122\n', '0.910,0.240,0.119\n', '0.915,0.237,0.115\n',
                    '0.919,0.233,0.112\n', '0.923,0.229,0.108\n', '0.928,0.225,0.104\n', '0.932,0.222,0.100\n',
                    '0.937,0.217,0.096\n', '0.941,0.213,0.092\n', '0.945,0.209,0.088\n', '0.950,0.204,0.084\n',
                    '0.954,0.200,0.080\n', '0.959,0.195,0.075\n', '0.963,0.190,0.070\n', '0.967,0.185,0.066\n',
                    '0.972,0.179,0.060\n', '0.976,0.174,0.055\n', '0.981,0.168,0.050\n', '0.985,0.162,0.043\n',
                    '0.989,0.156,0.037\n', '0.994,0.149,0.030\n', '0.998,0.142,0.024\n', '1.000,0.134,0.018\n',
                    '1.000,0.126,0.012\n', '1.000,0.117,0.005\n', '1.000,0.108,0.000\n', '1.000,0.097,0.000\n']

    CET_C6_PATH = HOME_PATH + r"\CET-C6.csv"
    CET_D8_PATH = HOME_PATH + r"\CET-D8.csv"

    STD_SUFFIXES = BaseConfig.FILE_FILTER.split(";")
    LOT_TREE_HEAD = (
        STDF_HEAD.ID,
        STDF_HEAD.GROUP_TYPE,
        STDF_HEAD.GROUP_TEXT,
        STDF_HEAD.LOT_ID,
        STDF_HEAD.SBLOT_ID,
        STDF_HEAD.WAFER_ID,
        STDF_HEAD.BLUE_FILM_ID,
        STDF_HEAD.SUB_CON,
        STDF_HEAD.TEST_COD,
        STDF_HEAD.FLOW_ID,
        STDF_HEAD.QTY,
        STDF_HEAD.PASS,
        STDF_HEAD.YIELD,
        STDF_HEAD.PART_TYP,
        STDF_HEAD.JOB_NAM,
        STDF_HEAD.NODE_NAM,
        STDF_HEAD.SITE_CNT,
        STDF_HEAD.START_T,
        STDF_HEAD.FILE_NAME,
    )
    LOT_TREE_HEAD_LENGTH = len(LOT_TREE_HEAD)

    PART_FLAGS = PartFlags.PART_FLAGS
    FILE_TABLE_HEAD = (
        STDF_HEAD.READ_FAIL,
        STDF_HEAD.PART_FLAG,
        STDF_HEAD.MESSAGE,
        STDF_HEAD.LOT_ID,
        STDF_HEAD.SBLOT_ID,
        STDF_HEAD.WAFER_ID,
        STDF_HEAD.BLUE_FILM_ID,
        STDF_HEAD.TEST_COD,
        STDF_HEAD.SUB_CON,
        STDF_HEAD.FLOW_ID,
        STDF_HEAD.PART_TYP,
        STDF_HEAD.JOB_NAM,
        STDF_HEAD.NODE_NAM,
        STDF_HEAD.SETUP_T,
        STDF_HEAD.START_T,
        STDF_HEAD.TST_TEMP,
        STDF_HEAD.FILE_PATH,

    )
    SKIP_FILE_TABLE_DATA_HEAD = {
        STDF_HEAD.READ_FAIL,
        STDF_HEAD.PART_FLAG,
    }

    WEB_TABLE_HEAD = (
        STDF_HEAD.READ_FAIL,
        STDF_HEAD.PART_FLAG,
        STDF_HEAD.MESSAGE,
        STDF_HEAD.LOAD_RATE,
        STDF_HEAD.METHOD,
        STDF_HEAD.ID,
        STDF_HEAD.SUB_CON,
        STDF_HEAD.MODE_COD,
        STDF_HEAD.LOT_ID,
        STDF_HEAD.SBLOT_ID,
        STDF_HEAD.BLUE_FILM_ID,
        STDF_HEAD.WAFER_ID,
        STDF_HEAD.TEST_COD,
        STDF_HEAD.FLOW_ID,
        STDF_HEAD.PART_TYP,
        STDF_HEAD.JOB_NAM,
        STDF_HEAD.NODE_NAM,
        STDF_HEAD.TST_TEMP,
        STDF_HEAD.START_T,
        STDF_HEAD.FINISH_T,
    )

    # Save memory
    PRR_TYPE_DICT = PRR_HEAD.PRR_TYPE_DICT
    DTP_TYPE_DICT = DTP_HEAD.DTP_TYPE_DICT
    PTMD_TYPE_DICT = PTMD_HEAD.PTMD_TYPE_DICT
    BIN_TYPE_DICT = BIN_HEAD.BIN_TYPE_DICT
    DTR_TYPE_DICT = DTR_HEAD.DTR_TYPE_DICT

    JMP_SCRIPT_HEAD = [
        STDF_HEAD.GROUP, PRR_HEAD.DA_GROUP, PRR_HEAD.PART_ID, PRR_HEAD.PART_TXT, PRR_HEAD.X_COORD, PRR_HEAD.Y_COORD,
        PRR_HEAD.HARD_BIN, PRR_HEAD.SOFT_BIN, PRR_HEAD.FAIL_FLAG, PTMD_HEAD.TEXT, PRR_HEAD.TEST_T,
    ]
    LOGGER_TXT_HEAD = [
        STDF_HEAD.GROUP, PRR_HEAD.DA_GROUP, PRR_HEAD.PART_ID, PRR_HEAD.PART_TXT, PRR_HEAD.X_COORD, PRR_HEAD.Y_COORD,
        PRR_HEAD.HARD_BIN, PRR_HEAD.SOFT_BIN, PRR_HEAD.FAIL_FLAG, DTR_HEAD.LOGGER,
    ]
    CSV_LIMIT_START_COLUMN = 11

    RECORD_RE_DOWNLOAD = ("NO_CHANGE", "RE_SCAN")
    RECORD_NOT_EDIT_COLUMN = 6
    RECORD_HEAD = ("UPDATE", "NEED_RE_SCAN", "ID", "IS_SUCCESS", "ERR_MESSAGE", "CONFIG_NAME", "STDF_ALIAS",
                   STDF_HEAD.LOT_ID,
                   STDF_HEAD.SBLOT_ID,
                   STDF_HEAD.BLUE_FILM_ID,
                   STDF_HEAD.WAFER_ID,
                   STDF_HEAD.TEST_COD,
                   STDF_HEAD.FLOW_ID,
                   STDF_HEAD.PART_TYP,
                   STDF_HEAD.JOB_NAM,
                   STDF_HEAD.NODE_NAM,
                   STDF_HEAD.TST_TEMP,
                   STDF_HEAD.MODE_COD,
                   )

    DIE_ID_ADD = 1000000

    TEST_ID_COLUMN = 0
    TEST_TYPE_COLUMN = 1
    TEST_NUM_COLUMN = 2
    TEST_TXT_COLUMN = 3
    UNIT_COLUMN = 4
    LO_LIMIT_COLUMN = 5
    HI_LIMIT_COLUMN = 6
    AVG_COLUMN = 7
    STD_COLUMN = 8
    CPK_COLUMN = 9
    TOP_FAIL_COLUMN = 12
    REJECT_COLUMN = 14
    LO_LIMIT_TYPE_COLUMN = 18
    HI_LIMIT_TYPE_COLUMN = 19

    SOFT_BIN_COLUMN = 23
    SOFT_BIN_NAME_COLUMN = 24
    HARD_BIN_COLUMN = 25
    HARD_BIN_NAME_COLUMN = 26

    # Table中的小数点精确位要确认下
    SKIP_NEW_LIMIT_UNITS: list = ["BIT", "PAT"]

    PARSER_FILES = ("StdfTempPrr.csv", "StdfTempDtp.csv", "StdfTempPtmd.csv", "BinName.csv", "DatalogTxt.csv")
    PRR_FILE = "StdfTempPrr.csv"
    DTP_PATH = "StdfTempDtp.csv"
    PTMD_PATH = "StdfTempPtmd.csv"
    BIN_PATH = "BinName.csv"
    LOG_PATH = "DatalogTxt.csv"

    @staticmethod
    def init():
        if not os.path.exists(GlobalVariable.CACHE_PATH):
            os.makedirs(GlobalVariable.CACHE_PATH)
        if not os.path.exists(GlobalVariable.JMP_CACHE_PATH):
            os.makedirs(GlobalVariable.JMP_CACHE_PATH)
        if not os.path.exists(GlobalVariable.LIMIT_PATH):
            os.makedirs(GlobalVariable.LIMIT_PATH)
        if not os.path.exists(GlobalVariable.NGINX_PATH):
            os.makedirs(GlobalVariable.NGINX_PATH)
        if not os.path.exists(GlobalVariable.SWAP_PATH):
            os.makedirs(GlobalVariable.SWAP_PATH)
        if not os.path.exists(GlobalVariable.CET_C6_PATH):
            with open(GlobalVariable.CET_C6_PATH, "w") as f:
                f.writelines(GlobalVariable.CET_C6_COLOR)
        if not os.path.exists(GlobalVariable.CET_D8_PATH):
            with open(GlobalVariable.CET_D8_PATH, "w") as f:
                f.writelines(GlobalVariable.CET_D8_COLOR)


class TestVariable:
    """
    用于测试的各种路径
    """
    TEMP_PATH = r"D:\1_STDF\STDF_CACHE\DEMO_CP1"

    TEMP_PRR_PATH = os.path.join(TEMP_PATH, "StdfTempPrr.csv")
    TEMP_DTP_PATH = os.path.join(TEMP_PATH, "StdfTempDtp.csv")
    TEMP_PTMD_PATH = os.path.join(TEMP_PATH, "StdfTempPtmd.csv")
    TEMP_BIN_PATH = os.path.join(TEMP_PATH, "BinName.csv")
    TEMP_LOG_PATH = os.path.join(TEMP_PATH, "DatalogTxt.csv")

    PATHS = (TEMP_PRR_PATH, TEMP_DTP_PATH, TEMP_PTMD_PATH, TEMP_BIN_PATH, TEMP_LOG_PATH)

    # HDF5_PATH = os.path.join(GlobalVariable.CACHE_PATH, "TEST_DATA.h5")
    HDF5_PATH = r"D:\1_STDF\STDF_CACHE\DEMO_DATA.h5"
    HDF5_2_PATH = r"D:\1_STDF\STDF_CACHE\DEMO_FT.h5"

    TABLE_PICKLE_PATH = os.path.join(GlobalVariable.CACHE_PATH, '{}.pkl'.format("TABLE_DATA"))

    STDF_PATH = r"D:\SWAP\TE\DEMO_DATA.std"
    STDF_FILES_PATH = r"D:\SWAP\TE"


class GlobalRePattern:
    """
    用来放一些全局可用的Re Pattern
    """
