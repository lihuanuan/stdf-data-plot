#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : for_calculation.py
@Author  : Link
@Time    : 2023/3/26 11:54
@Mark    : 
"""
from dataclasses import dataclass
from typing import Union
from pydantic import ValidationError, BaseModel

import pandas as pd


@dataclass
class PtmdModuleWithCache:
    ptmd_df: pd.DataFrame
    cache_ids: dict


class Calculation(BaseModel):
    """
    暂未用到
    """
    TEST_ID: int
    DATAT_TYPE: str
    TEST_NUM: int
    TEST_TXT: str
    UNITS: str
    LO_LIMIT: float
    HI_LIMIT: float
    AVG: float
    STD: float
    CPK: float
    MEDIAN: float
    QTY: int
    FAIL_QTY: int
    FAIL_RATE: str
    REJECT_QTY: int
    REJECT_RATE: str
    MIN: float
    MAX: float
    LO_LIMIT_TYPE: Union[str, float]
    HI_LIMIT_TYPE: Union[str, float]
    ALL_DATA_MIN: float
    ALL_DATA_MAX: float
    TEXT: str
    SOFT_BIN: int
    SOFT_BIN_NAME: str
    HARD_BIN: int
    HARD_BIN_NAME: str


class TCalculation(BaseModel):
    data: Union[str, int]


if __name__ == '__main__':
    t = TCalculation(data="A")
    print(t.dict())
