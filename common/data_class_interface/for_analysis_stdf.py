#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : for_analysis_stdf.py
@Author  : Link
@Time    : 2023/3/26 11:53
@Mark    : 
"""
from dataclasses import dataclass
from enum import Enum

from numpy import (
    uint8 as U1,
    uint16 as U2,
    uint32 as U4,
    int8 as I1,
    int16 as I2,
    int32 as I4,
    float32 as R4,
    float64 as R8,
    nan
)


class STDF_HEAD:
    ID = "ID"

    SUB_CON = "SUB_CON"
    LOT_ID = "LOT_ID"
    SBLOT_ID = "SBLOT_ID"
    WAFER_ID = "WAFER_ID"
    BLUE_FILM_ID = "BLUE_FILM_ID"
    TEST_COD = "TEST_COD"
    FLOW_ID = "FLOW_ID"
    PART_TYP = "PART_TYP"
    JOB_NAM = "JOB_NAM"
    TST_TEMP = "TST_TEMP"
    SETUP_T = "SETUP_T"
    START_T = "START_T"
    FINISH_T = "FINISH_T"
    SITE_CNT = "SITE_CNT"
    NODE_NAM = "NODE_NAM"

    FILE_PATH = "FILE_PATH"
    FILE_NAME = "FILE_NAME"
    HDF5_PATH = "HDF5_PATH"
    READ_FAIL = "READ_FAIL"
    PART_FLAG = "PART_FLAG"
    MODE_COD = "MODE_COD"

    GROUP_TYPE = "GROUP_TYPE"
    GROUP_TEXT = "GROUP_TEXT"

    QTY = "QTY"
    PASS = "PASS"
    YIELD = "YIELD"
    DATA = "DATA"

    # FILE
    GROUP = "GROUP"  # running add
    ALL_GROUP = "ALL_GROUP"  # running add
    MESSAGE = "MESSAGE"  # running add
    LOAD_RATE = "LOAD_RATE"  # running add
    METHOD = "METHOD"  # running add
    # WEB
    RATE = "RATE"  # running add
    STATUS = "STATUS"  # running add
    DOWN = "DOWN"  # running add
    MIR_IDS = "MIR_IDS"  # running add


class PRR_HEAD:
    PART_ID = "PART_ID"
    PART_TXT = "PART_TXT"
    HEAD_NUM = "HEAD_NUM"
    SITE_NUM = "SITE_NUM"
    X_COORD = "X_COORD"
    Y_COORD = "Y_COORD"
    HARD_BIN = "HARD_BIN"
    SOFT_BIN = "SOFT_BIN"
    PART_FLG = "PART_FLG"
    NUM_TEST = "NUM_TEST"
    FAIL_FLAG = "FAIL_FLAG"
    TEST_T = "TEST_T"

    ID = "ID"  # running add
    DA_GROUP = "DA_GROUP"  # running add
    DIE_ID = "DIE_ID"  # running add
    FAIL_TEST_ID = "FAIL_TEST_ID"  # running add, REAL_TEST_ID

    PRR_HEAD = (PART_ID, PART_TXT, HEAD_NUM, SITE_NUM, X_COORD, Y_COORD, HARD_BIN, SOFT_BIN, PART_FLG,
                NUM_TEST, FAIL_FLAG, TEST_T)
    PRR_TYPE = (U4, str, U1, U1, I2, I2, U2, U2, U1, U2, U1, U4,)
    PRR_TYPE_DICT = dict(zip(PRR_HEAD, PRR_TYPE))


class DTP_HEAD:
    PART_ID = "PART_ID"
    TEST_ID = "TEST_ID"
    RESULT = "RESULT"
    TEST_FLG = "TEST_FLG"
    PARM_FLG = "PARM_FLG"
    OPT_FLAG = "OPT_FLAG"
    LO_LIMIT = "LO_LIMIT"
    HI_LIMIT = "HI_LIMIT"

    ID = "ID"  # running add
    FAIL_FLG = "FAIL_FLG"  # running add
    DIE_ID = "DIE_ID"  # running add
    NEW_TEST_ID = "NEW_TEST_ID"  # # running add 用来唯一

    # For HtmlReport
    DATAT_TYPE: str = ""
    TEST_NUM: int = 0
    TEST_TXT: str = ""
    UNITS: str = ""

    DTP_HEAD = (PART_ID, TEST_ID, RESULT, TEST_FLG, PARM_FLG, OPT_FLAG, LO_LIMIT, HI_LIMIT)
    DTP_TYPE = (U4, U2, R4, U1, U1, U1, R4, R4)
    DTP_TYPE_DICT = dict(zip(DTP_HEAD, DTP_TYPE))


class PTMD_HEAD:
    TEST_ID = "TEST_ID"
    DATAT_TYPE = "DATAT_TYPE"
    TEST_NUM = "TEST_NUM"
    TEST_TXT = "TEST_TXT"
    PARM_FLG = "PARM_FLG"
    OPT_FLAG = "OPT_FLAG"
    RES_SCAL = "RES_SCAL"
    LLM_SCAL = "LLM_SCAL"
    HLM_SCAL = "HLM_SCAL"
    LO_LIMIT = "LO_LIMIT"
    HI_LIMIT = "HI_LIMIT"
    UNITS = "UNITS"
    C_RESFMT = "C_RESFMT"
    C_LLMFMT = "C_LLMFMT"
    C_HLMFMT = "C_HLMFMT"
    LO_SPEC = "LO_SPEC"
    HI_SPEC = "HI_SPEC"

    ID = "ID"  # running add
    TEXT = "TEXT"  # running add
    NEW_TEST_ID = "NEW_TEST_ID"  # # running add 用来唯一
    # BK_TEST_ID = "BK_TEST_ID"
    FAIL_QTY = "FAIL_QTY"  # running add, top fail计数
    REJECT_QTY = "REJECT_QTY"
    QTY = "QTY"  # 当前测试项目测试的QTY

    TEST_ID_DROP = [TEXT, TEST_NUM, TEST_TXT, ]

    PTMD_HEAD = (TEST_ID, DATAT_TYPE, TEST_NUM, TEST_TXT, PARM_FLG, OPT_FLAG, RES_SCAL, LLM_SCAL,
                 HLM_SCAL, LO_LIMIT, HI_LIMIT, UNITS, C_RESFMT, C_LLMFMT, C_HLMFMT, LO_SPEC, HI_SPEC)
    PTMD_TYPE = (U2, str, U4, str, U1, U1, I1, I1, I1, R8, R8, str, str, str, str, R4, R4)
    PTMD_TYPE_DICT = dict(zip(PTMD_HEAD, PTMD_TYPE))


class BIN_HEAD:
    BIN_TYPE = "BIN_TYPE"
    BIN_NUM = "BIN_NUM"
    BIN_PF = "BIN_PF"
    BIN_NAM = "BIN_NAM"

    SBIN_PF = "SBIN_PF"
    SBIN_NAM = "SBIN_NAM"

    HBIN_PF = "HBIN_PF"
    HBIN_NAM = "HBIN_NAM"

    BIN_HEAD = (BIN_TYPE, BIN_NUM, BIN_PF, BIN_NAM)
    DATA_TYPE = (str, U2, str, str)
    BIN_TYPE_DICT = dict(zip(BIN_HEAD, DATA_TYPE))


class DTR_HEAD:
    PART_ID = "PART_ID"
    LOGGER = "LOGGER"

    DTR_HEAD = (PART_ID, LOGGER)
    DATA_TYPE = (U4, str)
    DTR_TYPE_DICT = dict(zip(DTR_HEAD, DATA_TYPE))


@dataclass
class PtmdModule:
    ID: int  # UI赋予的ID
    TEST_ID: int
    DATAT_TYPE: str
    TEST_NUM: int
    TEST_TXT: str
    PARM_FLG: int
    OPT_FLAG: int
    RES_SCAL: int
    LLM_SCAL: int
    HLM_SCAL: int
    LO_LIMIT: float
    HI_LIMIT: float
    UNITS: str
    TEXT: str
    NEW_TEST_ID: int
