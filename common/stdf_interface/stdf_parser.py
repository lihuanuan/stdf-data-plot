"""
-*- coding: utf-8 -*-
@Author  : Link
@Time    : 2021/12/13 20:20
@Software: PyCharm
@File    : stdf_parser.py
@Remark  : 
"""
import os
import struct

from Semi_ATE import STDF
from Semi_ATE.STDF.utils import create_record_object

from common.app_variable import GlobalVariable
from common.data_class_interface.for_analysis_stdf import STDF_HEAD, PRR_HEAD


class records_from_file(STDF.records_from_file):

    def __next__(self):
        while self.fd is not None:
            header = self.fd.read(4)
            if len(header) != 4:
                raise StopIteration
            else:
                REC_LEN, REC_TYP, REC_SUB = struct.unpack(self.fmt, header)
                footer = self.fd.read(REC_LEN)
                if (REC_TYP, REC_SUB) in self.records_of_interest:
                    if len(footer) != REC_LEN:
                        raise StopIteration()
                    else:
                        try:
                            return create_record_object(self.version, self.endian, (REC_TYP, REC_SUB), header + footer)
                        except Exception as err:
                            print(err)
                            return None


class SemiStdfUtils:
    @staticmethod
    def is_std(file_name):
        suffix = os.path.splitext(file_name)[-1]
        if suffix not in GlobalVariable.STD_SUFFIXES:
            return False
        return True

    @staticmethod
    def get_lot_info_by_semi_ate(filepath: str, **kwargs) -> dict:
        data_dict = {
            STDF_HEAD.FILE_PATH: filepath,
            **kwargs,
            STDF_HEAD.LOT_ID: "",
            STDF_HEAD.SBLOT_ID: "",
            STDF_HEAD.WAFER_ID: "",
            STDF_HEAD.BLUE_FILM_ID: "",
            STDF_HEAD.SUB_CON: 'NA',
            STDF_HEAD.TEST_COD: '',
            STDF_HEAD.FLOW_ID: '',
            STDF_HEAD.PART_TYP: '',
            STDF_HEAD.JOB_NAM: '',
            STDF_HEAD.TST_TEMP: '',
            STDF_HEAD.NODE_NAM: '',
            STDF_HEAD.SETUP_T: 0,
            STDF_HEAD.START_T: 0,
            STDF_HEAD.SITE_CNT: 0,
        }
        for REC in records_from_file(filepath):
            if REC is None: continue
            if REC.id == "PIR": break
            if REC.id == "MIR":
                mir = REC.to_dict()
                data_dict[STDF_HEAD.LOT_ID] = mir[STDF_HEAD.LOT_ID]
                data_dict[STDF_HEAD.SBLOT_ID] = mir[STDF_HEAD.SBLOT_ID]

                data_dict[STDF_HEAD.TEST_COD] = mir[STDF_HEAD.TEST_COD]
                data_dict[STDF_HEAD.FLOW_ID] = mir[STDF_HEAD.FLOW_ID]
                data_dict[STDF_HEAD.PART_TYP] = mir[STDF_HEAD.PART_TYP]
                data_dict[STDF_HEAD.JOB_NAM] = mir[STDF_HEAD.JOB_NAM]
                data_dict[STDF_HEAD.TST_TEMP] = mir[STDF_HEAD.TST_TEMP]
                data_dict[STDF_HEAD.NODE_NAM] = mir[STDF_HEAD.NODE_NAM]

                data_dict[STDF_HEAD.SETUP_T] = mir[STDF_HEAD.SETUP_T]
                data_dict[STDF_HEAD.START_T] = mir[STDF_HEAD.START_T]

            if REC.id == "WIR":
                wir = REC.to_dict()
                if wir[PRR_HEAD.HEAD_NUM] == 233:
                    data_dict[STDF_HEAD.BLUE_FILM_ID] = wir[STDF_HEAD.WAFER_ID]
                else:
                    data_dict[STDF_HEAD.WAFER_ID] = wir[STDF_HEAD.WAFER_ID]

            if REC.id == "WCR":
                pass

            if REC.id == "SDR":
                sdr = REC.to_dict()
                data_dict[STDF_HEAD.SITE_CNT] = sdr[STDF_HEAD.SITE_CNT]

        return data_dict
