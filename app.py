#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : app.py
@Author  : Link
@Time    : 2022/12/16 21:24
@Mark    :
@Version : V3.0
@START_T : 20220814
@RELEASE :
"""
import os
import shutil
import sys

from PySide2.QtCore import Signal, QObject
from PySide2.QtGui import QCloseEvent
from PySide2.QtWidgets import QApplication

from ui_component.ui_common.my_text_browser import UiMessage, Print
from ui_component.ui_main.ui_main import Application, Main_Ui

import warnings

warnings.filterwarnings("ignore")


class Stream(QObject):
    conn = True
    newText = Signal(str)

    def write(self, text):
        if not self.conn:
            return
        self.newText.emit(str(text))
        # 实时刷新界面
        QApplication.processEvents()


class main_ui(Main_Ui):
    terminator: str = "\r\n"
    receive_data: str = None

    def __init__(self, parent=None):
        super(main_ui, self).__init__(parent=parent)
        self.sd = Stream()
        self.se = Stream()
        self.sd.newText.connect(self.outputWritten)
        self.se.newText.connect(self.errorWritten)
        sys.stdout = self.sd
        sys.stderr = self.se

    def outputWritten(self, text: str):
        """
        根据规则来打印合适颜色的信息
        :param text:
        :return:
        """
        self.text_browser.split_append(text)

    def errorWritten(self, text: str):
        self.text_browser.m_append(
            UiMessage.Error(text)
        )

    def closeEvent(self, a0: QCloseEvent) -> None:
        sys.stdout.conn = False
        sys.stderr.conn = False
        sys.exit(0)


if __name__ == '__main__':
    import multiprocessing
    from ui_component.ui_app_variable import UiGlobalVariable
    from common.app_variable import GlobalVariable

    GlobalVariable.LICENSE_CONTROL = False
    GlobalVariable.init()
    UiGlobalVariable.GraphUseLocalColor = True
    multiprocessing.freeze_support()
    app = Application(sys.argv)
    app.setApplicationName("IC DATA ANALYSIS")

    # win = main_ui()
    # app.setWindowIcon(win.icon)
    # win.show()
    # sys.exit(app.exec_())
    version = "V3P7DemoRelease"
    version_path = os.path.join(GlobalVariable.HOME_PATH, version)
    try:
        win = main_ui()
        win.setWindowTitle("LinkPlot Demo -> License有效期 无限期")
        app.setWindowIcon(win.icon)
        win.show()
        win.m_append(UiMessage.Error(GlobalVariable.LICENSE_MESSAGE))
        if not os.path.exists(version_path):
            shutil.rmtree(GlobalVariable.CACHE_PATH)
            GlobalVariable.init()
            Print.Warning("App版本更新, 删除历史缓存数据!")
        with open(version_path, 'w') as _:
            pass
        sys.exit(app.exec_())
    except Exception as err:
        sys.stdout = None
        print(err)
    finally:
        print('Program is dead.')
