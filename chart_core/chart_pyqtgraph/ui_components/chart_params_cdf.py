#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : chart_params_cdf.py
@Author  : Link
@Time    : 2023/5/15 21:43
@Mark    : CDF图, 可以不考虑Select, 或是Select后跟随着数据改变

import numpy as np
import matplotlib.pyplot as plt

dx = 0.005
x  = np.arange(-10, 10, dx)
y  = 0.25*np.exp((-x ** 2)/8)

y=y/ (np.sum(dx * y))
cdf = np.cumsum(y * dx)

plt.plot(x,y,label="pdf")
plt.plot(x,cdf,label="cdf")
plt.xlabel("X")
plt.ylabel("Probability Values")
plt.title("CDF for continuous distribution")
plt.legend()
plt.show()

"""
