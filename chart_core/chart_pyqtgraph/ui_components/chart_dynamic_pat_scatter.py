#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : chart_dynamic_pat_scatter.py
@Author  : Link
@Time    : 2023/5/30 22:18
@Mark    :
    总算开坑了
        1. By Group 分为多个子图, 不类似VisualMap一样, 就多几行绘图
            1.1 同步更新下ChartGroup
        2. 启动这个图的时候, Check并生成DPAT数据
        3. 不对数据做太多判定和操作, 由软件操作者来确认是否有DynamicPat数据 -> 减少开发难度
        4. 会复杂点, 数据选取功能是否还在?, 可能需要在CustomViewBox中写入Group名字, 传出来的信号带上GroupName
        5. 继承自TransScatterChart

        # from functools import partial  # https://blog.csdn.net/qq_33688922/article/details/91890142
        # each.clicked.connect(partial(self.changeAddNum, each.text()))
        # @pyqtSlot(str)
        # def changeAddNum(self, text):
        #     self.lcdNumber.display(int(text))
"""

import math
from typing import Union, List

import numpy as np
import pandas as pd

from PySide2 import QtCore
from PySide2.QtGui import QResizeEvent, QCloseEvent
from pyqtgraph import ScatterPlotItem, InfiniteLine

from chart_core.chart_pyqtgraph.core.mixin import BasePlot, GraphRangeSignal, PlotWidget
from chart_core.chart_pyqtgraph.core.view_box import CustomViewBox, pg
from chart_core.chart_pyqtgraph.ui_components.ui_unit_chart import UnitChartWindow
from common.li import Li
from ui_component.ui_app_variable import UiGlobalVariable
