#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : chart_utils.py
@Author  : Link
@Time    : 2023/3/26 15:19
@Mark    : 
"""

from pydantic import ValidationError, BaseModel


class ChartParams(BaseModel):
    LO_LIMIT: float
    HI_LIMIT: float
    INC: float
    INC_X2: float
    MIN: float
    MAX: float
    DECIMAL: int


class ChartUtils:
    @staticmethod
    def get_for_qt_params(cpk_info: dict, ):
        pass

    @staticmethod
    def get_for_jmp_params(cpk_info: dict, ):
        pass
