# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'file_load.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from ui_component.ui_module.table_module import QtTableWidget
from ui_component.ui_module.combox import ComboCheckBox


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(1018, 481)
        self.verticalLayout = QVBoxLayout(Form)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.tableWidget = QtTableWidget(Form)
        self.tableWidget.setObjectName(u"tableWidget")

        self.gridLayout_2.addWidget(self.tableWidget, 0, 0, 1, 1)


        self.verticalLayout.addLayout(self.gridLayout_2)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.pushButton = QPushButton(Form)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setMaximumSize(QSize(40, 16777215))

        self.horizontalLayout_2.addWidget(self.pushButton)

        self.pushButton_5 = QPushButton(Form)
        self.pushButton_5.setObjectName(u"pushButton_5")
        self.pushButton_5.setMaximumSize(QSize(40, 16777215))

        self.horizontalLayout_2.addWidget(self.pushButton_5)

        self.pushButton_6 = QPushButton(Form)
        self.pushButton_6.setObjectName(u"pushButton_6")
        self.pushButton_6.setMaximumSize(QSize(40, 16777215))

        self.horizontalLayout_2.addWidget(self.pushButton_6)

        self.pushButton_2 = QPushButton(Form)
        self.pushButton_2.setObjectName(u"pushButton_2")
        self.pushButton_2.setMaximumSize(QSize(60, 16777215))

        self.horizontalLayout_2.addWidget(self.pushButton_2)

        self.pushButton_3 = QPushButton(Form)
        self.pushButton_3.setObjectName(u"pushButton_3")
        self.pushButton_3.setMaximumSize(QSize(60, 16777215))

        self.horizontalLayout_2.addWidget(self.pushButton_3)

        self.pushButton_4 = QPushButton(Form)
        self.pushButton_4.setObjectName(u"pushButton_4")
        self.pushButton_4.setMaximumSize(QSize(60, 16777215))

        self.horizontalLayout_2.addWidget(self.pushButton_4)

        self.label = QLabel(Form)
        self.label.setObjectName(u"label")

        self.horizontalLayout_2.addWidget(self.label)

        self.comboBox = QComboBox(Form)
        self.comboBox.setObjectName(u"comboBox")

        self.horizontalLayout_2.addWidget(self.comboBox)

        self.label_2 = QLabel(Form)
        self.label_2.setObjectName(u"label_2")

        self.horizontalLayout_2.addWidget(self.label_2)

        self.comboBox_2 = ComboCheckBox(Form)
        self.comboBox_2.setObjectName(u"comboBox_2")
        self.comboBox_2.setMinimumSize(QSize(200, 0))

        self.horizontalLayout_2.addWidget(self.comboBox_2)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.progressBar = QProgressBar(Form)
        self.progressBar.setObjectName(u"progressBar")
        self.progressBar.setMaximumSize(QSize(100, 16777215))
        self.progressBar.setValue(0)
        self.progressBar.setTextVisible(False)

        self.horizontalLayout_2.addWidget(self.progressBar)


        self.verticalLayout.addLayout(self.horizontalLayout_2)


        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))
#if QT_CONFIG(tooltip)
        self.pushButton.setToolTip(QCoreApplication.translate("Form", u"120010STDF\u6570\u636e\u6587\u4ef6\u8f7d\u5165", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton.setText(QCoreApplication.translate("Form", u"\u8f7d\u5165", None))
#if QT_CONFIG(tooltip)
        self.pushButton_5.setToolTip(QCoreApplication.translate("Form", u"120020STDF\u6570\u636e\u6587\u4ef6\u91cd\u65b0\u8f7d\u5165", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_5.setText(QCoreApplication.translate("Form", u"\u91cd\u8f7d", None))
#if QT_CONFIG(tooltip)
        self.pushButton_6.setToolTip(QCoreApplication.translate("Form", u"120030\u6e05\u7a7aLoad\u6587\u4ef6", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_6.setText(QCoreApplication.translate("Form", u"\u6e05\u7a7a", None))
#if QT_CONFIG(tooltip)
        self.pushButton_2.setToolTip(QCoreApplication.translate("Form", u"120040\u6dfb\u52a0STDF\u6570\u636e\u6587\u4ef6", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_2.setText(QCoreApplication.translate("Form", u"\u6dfb\u52a0\u6587\u4ef6", None))
#if QT_CONFIG(tooltip)
        self.pushButton_3.setToolTip(QCoreApplication.translate("Form", u"120050\u6240\u6709\u7684Load\u6587\u4ef6\u6807\u8bb0\u4e3a\u8bfb\u53d6Fail\u6570\u636e", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_3.setText(QCoreApplication.translate("Form", u"\u6240\u6709\u6807R", None))
#if QT_CONFIG(tooltip)
        self.pushButton_4.setToolTip(QCoreApplication.translate("Form", u"120060\u6240\u6709\u7684Load\u6587\u4ef6\u6807\u8bb0\u4e3a\u4e0d\u53bb\u8bfb\u53d6Fail\u6570\u636e", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_4.setText(QCoreApplication.translate("Form", u"\u53d6\u6d88\u6807R", None))
#if QT_CONFIG(tooltip)
        self.label.setToolTip(QCoreApplication.translate("Form", u"120070\u5bf9\u5355\u4e2a\u6587\u4ef6\u7684\u6570\u636e\u53bb\u91cd\u6a21\u578b", None))
#endif // QT_CONFIG(tooltip)
        self.label.setText(QCoreApplication.translate("Form", u"PART\u3000MODE", None))
#if QT_CONFIG(tooltip)
        self.label_2.setToolTip(QCoreApplication.translate("Form", u"120080\u8bbe\u7f6eLoadStdf\u9ed8\u8ba4\u7684\u5206\u7ec4\u89c4\u5219", None))
#endif // QT_CONFIG(tooltip)
        self.label_2.setText(QCoreApplication.translate("Form", u"GROUP_TYPE", None))
        self.progressBar.setFormat("")
    # retranslateUi


    def retranslate(self):
        self.pushButton.setToolTip(QCoreApplication.translate("Form", u"120010Stdf Data File Loading", None))
        self.pushButton.setText(QCoreApplication.translate("Form", u"Load", None))
        self.pushButton_5.setToolTip(QCoreApplication.translate("Form", u"120020Stdf Data File Reload", None))
        self.pushButton_5.setText(QCoreApplication.translate("Form", u"Overload", None))
        self.pushButton_6.setToolTip(QCoreApplication.translate("Form", u"120030 Empty Load File", None))
        self.pushButton_6.setText(QCoreApplication.translate("Form", u"Empty", None))
        self.pushButton_2.setToolTip(QCoreApplication.translate("Form", u"120040 Add Stdf Data File", None))
        self.pushButton_2.setText(QCoreApplication.translate("Form", u"Add Files", None))
        self.pushButton_3.setToolTip(QCoreApplication.translate("Form", u"120050 All Load Files Are Marked To Read Fail Data", None))
        self.pushButton_3.setText(QCoreApplication.translate("Form", u"All Standard R", None))
        self.pushButton_4.setToolTip(QCoreApplication.translate("Form", u"120060 All Load Files Are Marked Not To Read Fail Data", None))
        self.pushButton_4.setText(QCoreApplication.translate("Form", u"Unmark R", None))
        self.label.setToolTip(QCoreApplication.translate("Form", u"120070 Data Deduplication Model For Individual Files", None))
        self.label.setText(QCoreApplication.translate("Form", u"Part Mode", None))
        self.label_2.setToolTip(QCoreApplication.translate("Form", u"120080 Set Loadstdf Default Grouping Rules", None))
        self.label_2.setText(QCoreApplication.translate("Form", u"Group_Type", None))
