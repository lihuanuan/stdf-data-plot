# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'table_load.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import pyqtsource_rc

class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(1221, 503)
        self.verticalLayout = QVBoxLayout(Form)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")

        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.pushButton = QPushButton(Form)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setEnabled(True)
        icon = QIcon()
        icon.addFile(u":/pyqt/source/images/lc_runmacro.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton.setIcon(icon)

        self.horizontalLayout_2.addWidget(self.pushButton)

        self.pushButton_2 = QPushButton(Form)
        self.pushButton_2.setObjectName(u"pushButton_2")
        self.pushButton_2.setEnabled(True)
        icon1 = QIcon()
        icon1.addFile(u":/pyqt/source/images/lc_gluehorzaligncenter.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_2.setIcon(icon1)

        self.horizontalLayout_2.addWidget(self.pushButton_2)

        self.pushButton_5 = QPushButton(Form)
        self.pushButton_5.setObjectName(u"pushButton_5")
        icon2 = QIcon()
        icon2.addFile(u":/pyqt/source/images/lc_glueinsertpoint.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_5.setIcon(icon2)

        self.horizontalLayout_2.addWidget(self.pushButton_5)

        self.pushButton_4 = QPushButton(Form)
        self.pushButton_4.setObjectName(u"pushButton_4")
        self.pushButton_4.setEnabled(True)
        icon3 = QIcon()
        icon3.addFile(u":/pyqt/source/images/lc_texttocolumns.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_4.setIcon(icon3)

        self.horizontalLayout_2.addWidget(self.pushButton_4)

        self.pushButton_10 = QPushButton(Form)
        self.pushButton_10.setObjectName(u"pushButton_10")
        icon4 = QIcon()
        icon4.addFile(u":/pyqt/source/images/deletecolumns.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_10.setIcon(icon4)

        self.horizontalLayout_2.addWidget(self.pushButton_10)

        self.pushButton_7 = QPushButton(Form)
        self.pushButton_7.setObjectName(u"pushButton_7")
        icon5 = QIcon()
        icon5.addFile(u":/pyqt/source/images/lc_textdirectionlefttoright.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_7.setIcon(icon5)

        self.horizontalLayout_2.addWidget(self.pushButton_7)

        self.pushButton_8 = QPushButton(Form)
        self.pushButton_8.setObjectName(u"pushButton_8")
        icon6 = QIcon()
        icon6.addFile(u":/pyqt/source/images/lc_textdirectiontoptobottom.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_8.setIcon(icon6)

        self.horizontalLayout_2.addWidget(self.pushButton_8)

        self.pushButton_9 = QPushButton(Form)
        self.pushButton_9.setObjectName(u"pushButton_9")
        icon7 = QIcon()
        icon7.addFile(u":/pyqt/source/images/lc_showinvalid.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_9.setIcon(icon7)

        self.horizontalLayout_2.addWidget(self.pushButton_9)

        self.pushButton_6 = QPushButton(Form)
        self.pushButton_6.setObjectName(u"pushButton_6")
        icon8 = QIcon()
        icon8.addFile(u":/pyqt/source/images/lc_calculate.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_6.setIcon(icon8)

        self.horizontalLayout_2.addWidget(self.pushButton_6)

        self.label = QLabel(Form)
        self.label.setObjectName(u"label")
        self.label.setFrameShape(QFrame.Box)
        self.label.setFrameShadow(QFrame.Sunken)
        self.label.setLineWidth(1)

        self.horizontalLayout_2.addWidget(self.label)

        self.lineEdit_2 = QLineEdit(Form)
        self.lineEdit_2.setObjectName(u"lineEdit_2")
        self.lineEdit_2.setMaximumSize(QSize(150, 16777215))

        self.horizontalLayout_2.addWidget(self.lineEdit_2)

        self.pushButton_3 = QPushButton(Form)
        self.pushButton_3.setObjectName(u"pushButton_3")
        icon9 = QIcon()
        icon9.addFile(u":/pyqt/source/images/lc_recsearch.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_3.setIcon(icon9)

        self.horizontalLayout_2.addWidget(self.pushButton_3)

        self.lineEdit = QLineEdit(Form)
        self.lineEdit.setObjectName(u"lineEdit")

        self.horizontalLayout_2.addWidget(self.lineEdit)

        self.checkBox = QCheckBox(Form)
        self.checkBox.setObjectName(u"checkBox")

        self.horizontalLayout_2.addWidget(self.checkBox)

        self.progressBar = QProgressBar(Form)
        self.progressBar.setObjectName(u"progressBar")
        self.progressBar.setMaximumSize(QSize(100, 16777215))
        self.progressBar.setValue(0)
        self.progressBar.setTextVisible(False)

        self.horizontalLayout_2.addWidget(self.progressBar)


        self.verticalLayout.addLayout(self.horizontalLayout_2)


        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))
#if QT_CONFIG(tooltip)
        self.pushButton.setToolTip(QCoreApplication.translate("Form", u"140010\u6539\u53d8Limit\u540e\u91cd\u7b97\u6d4b\u8bd5Rate", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton.setText("")
#if QT_CONFIG(tooltip)
        self.pushButton_2.setToolTip(QCoreApplication.translate("Form", u"140020\u53ea\u770b\u9009\u4e2d\u9879\u76eePASS\u6570\u636e", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_2.setText("")
#if QT_CONFIG(tooltip)
        self.pushButton_5.setToolTip(QCoreApplication.translate("Form", u"140030\u53ea\u770b\u9009\u4e2d\u9879\u76eeFAIL\u6570\u636e", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_5.setText("")
#if QT_CONFIG(tooltip)
        self.pushButton_4.setToolTip(QCoreApplication.translate("Form", u"140040\u53ea\u770b\u9009\u53d6\u884c\u7684\u8ba1\u7b97", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_4.setText("")
#if QT_CONFIG(tooltip)
        self.pushButton_10.setToolTip(QCoreApplication.translate("Form", u"140050\u6392\u9664\u5e76\u5220\u9664\u9009\u53d6\u5217", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_10.setText("")
#if QT_CONFIG(tooltip)
        self.pushButton_7.setToolTip(QCoreApplication.translate("Form", u"140060\u5c06\u6b64\u5217\u7684\u6570\u636e\u66ff\u6362\u4e3a\u5750\u6807\u7684X", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_7.setText("")
#if QT_CONFIG(tooltip)
        self.pushButton_8.setToolTip(QCoreApplication.translate("Form", u"140070\u5c06\u6b64\u5217\u7684\u6570\u636e\u66ff\u6362\u4e3aY", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_8.setText("")
#if QT_CONFIG(tooltip)
        self.pushButton_9.setToolTip(QCoreApplication.translate("Form", u"140080\u5c06\u9009\u53d6\u7684\u6570\u636e\u901a\u8fc7\u81ea\u5b9a\u4e49\u7ec4\u5408\u65b9\u5f0f\u751f\u6210UUID", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_9.setText("")
#if QT_CONFIG(tooltip)
        self.pushButton_6.setToolTip(QCoreApplication.translate("Form", u"140090\u4e00\u952e\u6309\u7167AVG\u00b1Std\u66f4\u65b0Limit, \u5e76\u6267\u884c\u5237\u65b0", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_6.setText("")
#if QT_CONFIG(tooltip)
        self.label.setToolTip(QCoreApplication.translate("Form", u"140100\u586b\u5165\u9700\u8981\u7b5b\u9009\u4e0d\u66f4\u65b0Limit\u7684\u6d4b\u8bd5\u9879\u76ee\u5355\u4f4d", None))
#endif // QT_CONFIG(tooltip)
        self.label.setText(QCoreApplication.translate("Form", u"SKIP UNITS", None))
#if QT_CONFIG(tooltip)
        self.pushButton_3.setToolTip(QCoreApplication.translate("Form", u"140110\u5728\u53f3\u4fa7\u641c\u7d22\u6846\u5185\u641c\u7d22\u60f3\u8981\u770b\u7684\u9879\u76ee, \u652f\u6301\u6b63\u5219\u8868\u8fbe\u5f0f", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_3.setText("")
        self.lineEdit.setPlaceholderText(QCoreApplication.translate("Form", u"\u56de\u8f66\u7b5b\u9009", None))
#if QT_CONFIG(tooltip)
        self.checkBox.setToolTip(QCoreApplication.translate("Form", u"140120\u9ed8\u8ba4TopFail\u663e\u793a", None))
#endif // QT_CONFIG(tooltip)
        self.checkBox.setText(QCoreApplication.translate("Form", u"Top Fail \u663e\u793a", None))
    # retranslateUi


    def retranslate(self):
        self.pushButton.setToolTip(QCoreApplication.translate("Form", u"140010 Recalculate Test Rate After Changing Limit", None))
        self.pushButton_2.setToolTip(QCoreApplication.translate("Form", u"140020 Only Look At The Selected Item Pass Data", None))
        self.pushButton_5.setToolTip(QCoreApplication.translate("Form", u"140030 Look Only At The Selected Item'S Fail Data", None))
        self.pushButton_4.setToolTip(QCoreApplication.translate("Form", u"140040 Looking Only At The Calculation Of The Selected Rows", None))
        self.pushButton_10.setToolTip(QCoreApplication.translate("Form", u"140050 Exclude And Delete Picked Columns", None))
        self.pushButton_7.setToolTip(QCoreApplication.translate("Form", u"140060 Replace The Data For This Column With The X Of The Coordinates", None))
        self.pushButton_8.setToolTip(QCoreApplication.translate("Form", u"140070 Replace The Data In This Column With Y", None))
        self.pushButton_9.setToolTip(QCoreApplication.translate("Form", u"140080 Generate Uuids For Selected Data Through Custom Combinations", None))
        self.pushButton_6.setToolTip(QCoreApplication.translate("Form", u"140090 One-Click Press Avg ± Std To Update Limit And Perform A Refresh", None))
        self.label.setToolTip(QCoreApplication.translate("Form", u"140100 Fill In The Test Project Unit That Needs To Be Filtered Without Updating The Limit", None))
        self.label.setText(QCoreApplication.translate("Form", u"Skip Units", None))
        self.pushButton_3.setToolTip(QCoreApplication.translate("Form", u"140110 Search For The Item You Want To See In The Search Box On The Right, Support Regular Expressions", None))
        self.lineEdit.setPlaceholderText(QCoreApplication.translate("Form", u"Enter Screening", None))
        self.checkBox.setToolTip(QCoreApplication.translate("Form", u"140120 Default Topfail Display", None))
        self.checkBox.setText(QCoreApplication.translate("Form", u"Top Fail Display", None))
