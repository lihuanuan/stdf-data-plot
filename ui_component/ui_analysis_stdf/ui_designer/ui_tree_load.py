# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'tree_load.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(904, 490)
        self.verticalLayout = QVBoxLayout(Form)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.treeWidget = QTreeWidget(Form)
        __qtreewidgetitem = QTreeWidgetItem()
        __qtreewidgetitem.setText(0, u"1");
        self.treeWidget.setHeaderItem(__qtreewidgetitem)
        self.treeWidget.setObjectName(u"treeWidget")

        self.horizontalLayout_2.addWidget(self.treeWidget)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.pushButton = QPushButton(Form)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout.addWidget(self.pushButton)

        self.pushButton_2 = QPushButton(Form)
        self.pushButton_2.setObjectName(u"pushButton_2")
        self.pushButton_2.setEnabled(True)

        self.horizontalLayout.addWidget(self.pushButton_2)

        self.line_2 = QFrame(Form)
        self.line_2.setObjectName(u"line_2")
        self.line_2.setFrameShape(QFrame.VLine)
        self.line_2.setFrameShadow(QFrame.Sunken)

        self.horizontalLayout.addWidget(self.line_2)

        self.checkBox = QCheckBox(Form)
        self.checkBox.setObjectName(u"checkBox")
        self.checkBox.setEnabled(True)

        self.horizontalLayout.addWidget(self.checkBox)

        self.spinBox = QSpinBox(Form)
        self.spinBox.setObjectName(u"spinBox")
        self.spinBox.setEnabled(True)
        self.spinBox.setMinimum(500)
        self.spinBox.setMaximum(1000000)
        self.spinBox.setValue(2000)

        self.horizontalLayout.addWidget(self.spinBox)

        self.line_3 = QFrame(Form)
        self.line_3.setObjectName(u"line_3")
        self.line_3.setFrameShape(QFrame.VLine)
        self.line_3.setFrameShadow(QFrame.Sunken)

        self.horizontalLayout.addWidget(self.line_3)

        self.checkBox_2 = QCheckBox(Form)
        self.checkBox_2.setObjectName(u"checkBox_2")

        self.horizontalLayout.addWidget(self.checkBox_2)

        self.line = QFrame(Form)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.VLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.horizontalLayout.addWidget(self.line)

        self.label_2 = QLabel(Form)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setFrameShape(QFrame.NoFrame)
        self.label_2.setFrameShadow(QFrame.Plain)

        self.horizontalLayout.addWidget(self.label_2)

        self.comboBox_2 = QComboBox(Form)
        self.comboBox_2.setObjectName(u"comboBox_2")

        self.horizontalLayout.addWidget(self.comboBox_2)

        self.line_4 = QFrame(Form)
        self.line_4.setObjectName(u"line_4")
        self.line_4.setFrameShape(QFrame.VLine)
        self.line_4.setFrameShadow(QFrame.Sunken)

        self.horizontalLayout.addWidget(self.line_4)

        self.label = QLabel(Form)
        self.label.setObjectName(u"label")
        self.label.setEnabled(True)

        self.horizontalLayout.addWidget(self.label)

        self.comboBox = QComboBox(Form)
        self.comboBox.setObjectName(u"comboBox")
        self.comboBox.setEnabled(True)

        self.horizontalLayout.addWidget(self.comboBox)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.progressBar = QProgressBar(Form)
        self.progressBar.setObjectName(u"progressBar")
        self.progressBar.setMaximumSize(QSize(100, 16777215))
        self.progressBar.setValue(24)
        self.progressBar.setTextVisible(False)

        self.horizontalLayout.addWidget(self.progressBar)


        self.verticalLayout.addLayout(self.horizontalLayout)


        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))
#if QT_CONFIG(tooltip)
        self.pushButton.setToolTip(QCoreApplication.translate("Form", u"130010\u9009\u53d6\u7684\u6570\u636e\u8fdb\u884c\u8f7d\u5165", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton.setText(QCoreApplication.translate("Form", u"\u6570\u636e\u8f7d\u5165", None))
#if QT_CONFIG(tooltip)
        self.pushButton_2.setToolTip(QCoreApplication.translate("Form", u"130020\u591a\u4efd\u6570\u636e\u7ec4\u5408\u4e3a\u81ea\u5b9a\u4e49lot", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_2.setText(QCoreApplication.translate("Form", u"\u6570\u636e\u5408\u5e76", None))
#if QT_CONFIG(tooltip)
        self.checkBox.setToolTip(QCoreApplication.translate("Form", u"130030\u5bf9\u5355\u6761\u6570\u636e\u8fdb\u884c\u6570\u636e\u62bd\u6837\u8f7d\u5165", None))
#endif // QT_CONFIG(tooltip)
        self.checkBox.setText(QCoreApplication.translate("Form", u"\u62bd\u6837", None))
        self.spinBox.setSuffix(QCoreApplication.translate("Form", u"qty", None))
#if QT_CONFIG(tooltip)
        self.checkBox_2.setToolTip(QCoreApplication.translate("Form", u"130040\u4e0d\u8bfb\u53d6Pattern\u4ea7\u751f\u7684Funtion\u6570\u636e", None))
#endif // QT_CONFIG(tooltip)
        self.checkBox_2.setText(QCoreApplication.translate("Form", u"\u4e0d\u8bfb\u53d6FTR", None))
#if QT_CONFIG(tooltip)
        self.label_2.setToolTip(QCoreApplication.translate("Form", u"130050\u53d6\u51fa\u552f\u4e00\u7684TSET_ID(TEST_NO)", None))
#endif // QT_CONFIG(tooltip)
        self.label_2.setText(QCoreApplication.translate("Form", u"TEST_ID\u53bb\u91cd\u6a21\u578b", None))
#if QT_CONFIG(tooltip)
        self.label.setToolTip(QCoreApplication.translate("Form", u"130060\u5bf9\u4e8eGROUP_TYPE\u5206\u7ec4\u7684\u6570\u636e\u518d\u6b21\u53bb\u91cd", None))
#endif // QT_CONFIG(tooltip)
        self.label.setText(QCoreApplication.translate("Form", u"BY_TYPE_PART_FLAG", None))
    # retranslateUi


    def retranslate(self):
        self.pushButton.setToolTip(QCoreApplication.translate("Form", u"130010 Selected Data To Load", None))
        self.pushButton.setText(QCoreApplication.translate("Form", u"Data Loading", None))
        self.pushButton_2.setToolTip(QCoreApplication.translate("Form", u"130,020 + Data Combinations For Custom Lot", None))
        self.pushButton_2.setText(QCoreApplication.translate("Form", u"Data Merging", None))
        self.checkBox.setToolTip(QCoreApplication.translate("Form", u"130030 Data Sampling Loading Of A Single Piece Of Data", None))
        self.checkBox.setText(QCoreApplication.translate("Form", u"Sampling", None))
        self.spinBox.setSuffix(QCoreApplication.translate("Form", u"Qty", None))
        self.checkBox_2.setToolTip(QCoreApplication.translate("Form", u"130040 Do Not Read Funtion Data Generated By Pattern", None))
        self.checkBox_2.setText(QCoreApplication.translate("Form", u"Do Not Read Ftr", None))
        self.label_2.setToolTip(QCoreApplication.translate("Form", u"130050 Remove Unique Tset_Id (Test_No)", None))
        self.label_2.setText(QCoreApplication.translate("Form", u"Test_Id Deduplication Model", None))
        self.label.setToolTip(QCoreApplication.translate("Form", u"130060 Deduplicate Data Again For Group_Type Grouping", None))
        self.label.setText(QCoreApplication.translate("Form", u"By_Type_Part_Flag", None))
