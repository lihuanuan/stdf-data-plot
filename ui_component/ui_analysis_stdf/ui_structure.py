#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : ui_structure.py
@Author  : Link
@Time    : 2023/5/9 22:12
@Mark    : 
"""


class UiStdfStructure:
    Default = {
        'main':
            ('horizontal', [('dock', '124 DataGroupSet', {}), ('vertical', [('horizontal', [('vertical', [
                ('dock', '121 StdfFileSelect', {}), ('dock', '122 DataTreeSelect', {})], {'sizes': [136, 182]}),
                                                                                      (
                                                                                      'dock', '125 PythonConsole', {})],
                                                                       {'sizes': [882, 0]}),
                                                                      ('dock', '123 ItemDataAnalysis', {})],
                                                         {'sizes': [323, 406]})], {'sizes': [230, 882]}), 'float': []}

    HideLoadWithTree = {
        'main': ('horizontal', [('dock', '124 DataGroupSet', {}), ('vertical', [('horizontal', [('vertical', [
            ('dock', '121 StdfFileSelect', {}), ('dock', '122 DataTreeSelect', {})], {'sizes': [10, 10]}), (
                                                                                              'dock',
                                                                                              '125 PythonConsole',
                                                                                              {})],
                                                                           {'sizes': [1595, 0]}),
                                                                          ('dock', '123 ItemDataAnalysis', {})],
                                                             {'sizes': [0, 1276]})], {'sizes': [373, 1595]}),
        'float': []}

    OnlyTable = {
        'main': ('horizontal', [('dock', '124 DataGroupSet', {}), ('vertical', [('horizontal', [('vertical', [
            ('dock', '121 StdfFileSelect', {}), ('dock', '122 DataTreeSelect', {})], {'sizes': [10, 10]}), (
                                                                                              'dock',
                                                                                              '125 PythonConsole',
                                                                                              {})],
                                                                           {'sizes': [1548, 0]}),
                                                                          ('dock', '123 ItemDataAnalysis', {})],
                                                             {'sizes': [0, 821]})], {'sizes': [0, 1548]}), 'float': []}

    PythonConsole = {
        'main': ('horizontal', [('dock', '124 DataGroupSet', {}), ('vertical', [('horizontal', [('vertical', [
            ('dock', '121 StdfFileSelect', {}), ('dock', '122 DataTreeSelect', {})], {'sizes': [138, 138]}), (
                                                                                              'dock',
                                                                                              '125 PythonConsole',
                                                                                              {})],
                                                                           {'sizes': [0, 1082]}),
                                                                          ('dock', '123 ItemDataAnalysis', {})],
                                                             {'sizes': [281, 497]})], {'sizes': [220, 1087]}),
        'float': []}
