"""
-*- coding: utf-8 -*-
@Author  : Link
@Time    : 2022/5/9 17:44
@Software: PyCharm
@File    : ui_file_load_widget.py
@Remark  : 
"""
import os
import time
from dataclasses import dataclass

from PySide2.QtGui import QColor, QGuiApplication
from PySide2.QtWidgets import QWidget, QHeaderView, QFileDialog, QTableWidgetItem
from PySide2.QtCore import Qt, QThread, Signal, Slot

from typing import List, Set, Union

from common.app_variable import GlobalVariable, TestVariable, ReadFail
from common.data_class_interface.for_analysis_stdf import STDF_HEAD
from common.li import SummaryCore
from common.stdf_interface.stdf_parser import SemiStdfUtils
from parser_core.stdf_parser_file_write_read import ParserData
from ui_component.ui_analysis_stdf.ui_designer.ui_file_load import Ui_Form as FileLoadForm

from parser_core.dll_parser import LinkStdf
from ui_component.ui_app_variable import UiGlobalVariable
from ui_component.ui_common.my_text_browser import Print
from var_language import BaseConfig, language


@dataclass
class ThMessage:
    INDEX: int
    STATUS: int
    MESSAGE: str


class RunStdfAnalysis(QThread):
    """
    结束测试时间还是非常需要的, 考虑下只有在线版本才有这个功能是否会方便一些
    """
    stdf = None  # type:LinkStdf
    file_list = None  # type:List[dict]
    id:int = None
    by_analysis_list: list = None
    eventSignal = Signal(ThMessage)

    def __init__(self, parent=None):
        super(RunStdfAnalysis, self).__init__(parent)
        self.stdf = LinkStdf()
        self.stdf.init()
        self.reload = False
        self.id = 0

    def set_analysis_list(self, file_list):
        self.file_list = file_list

    def set_reload(self):
        self.reload = True

    def set_id(self, mid_nm):
        self.id = int(mid_nm * 1000)

    def run(self) -> None:
        if self.file_list is None:
            return
        self.by_analysis_list = []
        for index, each in enumerate(self.file_list):
            # try:
            """ 阻止不同的程序一起解析 """
            start = time.perf_counter()
            _, file_name = os.path.split(each[STDF_HEAD.FILE_PATH])
            stdf_name = file_name[:file_name.rfind('.')]
            save_path = os.path.join(GlobalVariable.CACHE_PATH, each[STDF_HEAD.LOT_ID])

            if not os.path.exists(save_path):
                os.mkdir(save_path)
            # 这边可以考虑使用STDF_NAME+INFO的模型
            hdf5_save_path = os.path.join(save_path, stdf_name + '.h5')
            if self.reload and os.path.exists(hdf5_save_path):
                os.remove(hdf5_save_path)
            if not os.path.exists(hdf5_save_path):
                self.eventSignal.emit(ThMessage(INDEX=index, STATUS=0, MESSAGE="开始解析STDF中!"))
                ParserData.delete_swap_file(save_path)
                boolean = self.stdf.parser_stdf_to_csv(each[STDF_HEAD.FILE_PATH], save_path)
                if not boolean:
                    if not BaseConfig.HAVE_LICENSE:
                        Print.Error(language.LicenseMessage)
                    else:
                        Print.Warning(language.DebugMessage)
                    self.eventSignal.emit(ThMessage(INDEX=index, STATUS=-1, MESSAGE="STDF文件解析失败!"))
                    continue
                df_module = ParserData.load_csv(save_path)
                if ParserData.save_hdf5(df_module, hdf5_save_path):
                    ParserData.delete_swap_file(save_path)
            else:
                self.eventSignal.emit(ThMessage(INDEX=index, STATUS=0, MESSAGE="缓存文件存在,调用缓存数据!"))

            """
            开始读取prr然后进行数据处理!
            """
            mdi_id = int(self.id + index)
            prr = ParserData.load_prr_df(hdf5_save_path, unit_id=mdi_id)
            by_analysis_data_dict = {
                **SemiStdfUtils.get_lot_info_by_semi_ate(each[STDF_HEAD.FILE_PATH], FILE_NAME=file_name, ID=mdi_id),
                **ParserData.get_yield(prr, each[STDF_HEAD.PART_FLAG], each[STDF_HEAD.READ_FAIL]),
                STDF_HEAD.PART_FLAG: each[STDF_HEAD.PART_FLAG],
                STDF_HEAD.READ_FAIL: ReadFail.Y if each[STDF_HEAD.READ_FAIL] else ReadFail.N,
                STDF_HEAD.HDF5_PATH: hdf5_save_path,
                STDF_HEAD.GROUP_TYPE: "|".join(UiGlobalVariable.DEFAULT_SELECT_BASE_GROUP),
                STDF_HEAD.SUB_CON: "NA",
            }
            by_analysis_data_dict[STDF_HEAD.GROUP_TEXT] = by_analysis_data_dict[STDF_HEAD.LOT_ID]  # 给一个默认的值 LOT_ID
            GROUP_TEXT_LIST = [by_analysis_data_dict[_type] for _type in UiGlobalVariable.DEFAULT_SELECT_BASE_GROUP]
            by_analysis_data_dict[STDF_HEAD.GROUP_TEXT] = "|".join(GROUP_TEXT_LIST)
            use_time = round(time.perf_counter() - start, 2)
            self.by_analysis_list.append(by_analysis_data_dict)
            self.eventSignal.emit(ThMessage(INDEX=index, STATUS=1, MESSAGE="STDF解析文件成功!用时{}s".format(use_time)))
        """数据整理OK"""
        self.eventSignal.emit(ThMessage(INDEX=len(self.file_list), STATUS=11, MESSAGE="数据解析完成"))
        self.reload = False


class FileLoadWidget(QWidget, FileLoadForm):
    """
    file select
    STDF文件 Load模块,需要使用一种线程池来解析STDF
    建议的做法, 为了节省内存, 做懒处理, FileLoad只解析, 然后提取关键数据给Tree, 减少内存的占用
    缺点就是载入的数据变长了
    """

    finished = Signal()  # 传送父子数据给TreeWidget
    closeSignal = Signal(int)

    th = None  # type:RunStdfAnalysis
    select_file = None  # type:Union[Set[str], None]
    summary: SummaryCore = None

    def __init__(self, summary: SummaryCore, parent=None, space_nm=1):
        super(FileLoadWidget, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle("STDF File Select")
        self.summary = summary
        self.space_nm = space_nm
        self.comboBox.addItems(GlobalVariable.PART_FLAGS)
        self.title = "STDF数据载入空间: {}".format(space_nm)
        self.th = RunStdfAnalysis(self)
        self.th.set_id(self.space_nm)
        self.th.finished.connect(self.th_finished)
        self.th.eventSignal.connect(self.th_message_event)
        self.setAttribute(Qt.WidgetAttribute.WA_StyledBackground)
        self.tableWidget.set_table_head(GlobalVariable.FILE_TABLE_HEAD)
        self.tableWidget.horizontalHeader().setSectionResizeMode(1, QHeaderView.ResizeToContents)
        self.comboBox_2.add_items(UiGlobalVariable.BASE_GROUP, default_select=UiGlobalVariable.DEFAULT_SELECT_BASE_GROUP)
        self.comboBox_2.itemChecked.connect(self.group_change)
        if BaseConfig.EN:
            self.retranslate()

    def group_change(self, data: list):
        UiGlobalVariable.DEFAULT_SELECT_BASE_GROUP = data

    def select_stdf(self) -> List[str]:
        """
        去重复, 读取STDF
        :return:
        """
        path_list, _ = QFileDialog.getOpenFileNames(self,
                                                    'Open Stdf File',
                                                    filter='stdf({})'.format(BaseConfig.FILE_FILTER),
                                                    # options=QFileDialog.DontUseNativeDialog,
                                                    )
        return path_list

    def select_stdf_directory(self) -> List[str]:
        """
        从整个文件夹下选取所有的文件
        """
        directory = QFileDialog.getExistingDirectory(self, "getExistingDirectory", "./")
        return self.scan_directory(directory)

    @staticmethod
    def scan_directory(directory):
        path_list = []
        for root, dirs, files in os.walk(directory):
            for file in files:
                suffix = "*" + os.path.splitext(file)[-1]
                if suffix.lower() in GlobalVariable.STD_SUFFIXES:
                    path_list.append(os.path.join(root, file))
        return path_list

    def first_select(self):
        """
        初始化选择
        :return:
        """
        self.select_file = set()
        select_stdf = self.select_stdf()
        if len(select_stdf) == 0:
            return Print.Warning("无文件被选取!")
        self.select_file = set(select_stdf)
        self.analysis_path_stdf_by_semi_ate()

    def first_directory_select(self):
        """
        初始化文件夹下文件选择
        """
        self.select_file = set()
        select_stdf = self.select_stdf_directory()
        if len(select_stdf) == 0:
            return Print.Warning("无文件被选取!")
        self.select_file = set(select_stdf)
        self.analysis_path_stdf_by_semi_ate()

    def directory_select_test(self, test_path):
        select_stdf = self.scan_directory(test_path)
        if len(select_stdf) == 0:
            return Print.Warning("无文件被选取!")
        self.select_file = set(select_stdf)
        self.analysis_path_stdf_by_semi_ate()

    def analysis_path_stdf_by_semi_ate(self):
        """
        执行数据载入, 并按照时间排序
        :return:
        """
        if not self.select_file:
            return Print.Warning("无文件被选取, 无法执行分析!")
        table_data = []
        for filepath in self.select_file:
            table_data.append(SemiStdfUtils.get_lot_info_by_semi_ate(filepath))
        table_data = sorted(table_data, key=lambda ev: ev[STDF_HEAD.SETUP_T])
        self.tableWidget.set_table_data(table_data)
        self.progressBar.setValue(0)
        self.progressBar.setMaximum(self.tableWidget.table_count)

    @Slot()
    def on_pushButton_2_pressed(self):
        """
        追加选择数据
        :return:
        """
        select_stdf = self.select_stdf()
        if len(select_stdf) == 0:
            return Print.Warning("无文件被选取!")
        if set(select_stdf) == self.select_file:
            return Print.Warning("文件结构未改变!")
        if self.select_file is None:
            self.select_file = set()
        self.select_file = self.select_file | set(select_stdf)
        self.analysis_path_stdf_by_semi_ate()

    @Slot()
    def on_pushButton_3_pressed(self):
        self.tableWidget.set_read_all_r(Qt.Checked)

    @Slot()
    def on_pushButton_4_pressed(self):
        self.tableWidget.set_read_all_r(Qt.Unchecked)

    @Slot()
    def on_pushButton_5_pressed(self):
        """
        先删除, 后运行
        """
        if self.tableWidget.table_count == 0:
            return Print.Warning("无文件结构被读取")
        self.th.set_reload()
        self.on_pushButton_pressed()

    @Slot()
    def on_pushButton_6_pressed(self):
        self.select_file = set()
        self.tableWidget.clearContents()

    @Slot(int)
    def on_comboBox_currentIndexChanged(self, e):
        self.tableWidget.set_all_part_flag(e)

    @Slot(ThMessage)
    def th_message_event(self, info: ThMessage):
        self.progressBar.setValue(info.INDEX)
        if info.INDEX == self.tableWidget.table_count:
            return Print.Info("{} > 完成数据解析!".format(self.title))
        # self.tableWidget.selectRow(index)
        item = QTableWidgetItem(info.MESSAGE)
        if info.STATUS == -1:
            item.setBackground(QColor(255, 110, 55))
        elif info.STATUS == 0:
            item.setBackground(QColor(180, 208, 201))
        else:
            item.setBackground(QColor(176, 255, 210))
        self.tableWidget.update_table_data(info.INDEX, STDF_HEAD.MESSAGE, item)

    def th_finished(self):
        """
        接收后台整理好的数据到前台调用
        :return:
        """
        self.summary.set_data(self.th.by_analysis_list)
        self.finished.emit()
        self.pushButton.setEnabled(True)
        self.pushButton_5.setEnabled(True)

    @Slot()
    def on_pushButton_pressed(self):
        """
        想要RUN之前得进行一些判定
        使用线程池进行数据处理
        可以给前台传一个Process
        给线程传入R, R版本才会分析Fail项目
        :return:
        """
        if self.tableWidget.table_count == 0:
            return Print.Warning("无文件结构被读取")
        self.tableWidget.update_temp_data()
        self.th.set_analysis_list(self.tableWidget.temp_table_data)
        self.th.start()
        self.pushButton.setEnabled(False)
        self.pushButton_5.setEnabled(False)

    def keyPressEvent(self, event):
        """ Ctrl + C复制表格内容 """
        if event.modifiers() == Qt.ControlModifier and event.key() == Qt.Key_C:
            # 获取表格的选中行
            ranges = self.tableWidget.selectedRanges()
            if not ranges:
                return
            selected_ranges = ranges[0]  # 只取第一个数据块,其他的如果需要要做遍历,简单功能就不写得那么复杂了
            text_str = "\t".join(GlobalVariable.FILE_TABLE_HEAD) + '\n'  # 最后总的内容
            # 行（选中的行信息读取）
            for row in range(selected_ranges.topRow(), selected_ranges.bottomRow() + 1):
                row_str = ""
                # 列（选中的列信息读取）
                for col in range(selected_ranges.leftColumn(), selected_ranges.rightColumn() + 1):
                    item = self.tableWidget.item(row, col)
                    if item is None:
                        row_str += '\t'
                        continue
                    row_str += item.text() + '\t'  # 制表符间隔数据
                text_str += row_str + '\n'  # 换行
            clipboard = QGuiApplication.clipboard()  # 获取剪贴板
            clipboard.setText(text_str)  # 内容写入剪贴板
