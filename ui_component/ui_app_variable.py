#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : ui_app_variable.py
@Author  : Link
@Time    : 2022/12/23 21:06
@Mark    : 控制信号
"""
import collections
from dataclasses import dataclass

from common.data_class_interface.for_analysis_stdf import STDF_HEAD, PRR_HEAD
from var_language import language, BaseConfig

ACTIONS_NAME = collections.namedtuple('Action', ["name", "text"])


@dataclass
class AppBus:
    code: int
    data: object = None


class UiAppCode:
    Ui = 0x20000

    DataAnalysis = 0x10000


class QtPlotAllUse:
    """
    需要在对外面的界面中被识别并修改全局变量
    MultiSelect = True:
        图形处于可多选状态
    """
    MultiSelect = False


class UiGlobalVariable:
    GraphUseLocalColor = False
    GraphPlotBin = True
    GraphBins = 60
    GraphScreen = 0
    GraphMeanAddSubSigma = 3
    GraphPlotColumn = 1
    GraphPlotScatterSimple = False
    GraphPlotScatterSimpleNum = 10000
    GraphPlotFloatRound = 9
    GraphPlotWidth = 1000
    GraphPlotHeight = 600
    GraphCpkLoClamp = -9999
    GraphCpkHiClamp = 1
    GraphTopFailClamp = 0
    GraphRejectClamp = 0
    PandasScreen = False
    PandasLogDefaultRow = 1000  # 默认显示Log的行数
    HtmlReportMaxRows: int = BaseConfig.HTML_REPORT_MAX_ROWS  # 默认显示Log的行数
    # GraphOnlyBinNM = False
    # --------------------------------------------------------------- 参数
    GRAPH_PARAMS = [
        {
            'name': "PyqtGraph绘图相关设定", 'type': 'group', 'children':
            [
                {'name': language.GraphSetting["GraphScreen"], 'type': 'list',
                 'value': GraphScreen,
                 'limits': {"LCL-To-UCL": 0, "PassMin-To-PassMax": 1, "DataMin-To-DataMax": 2, "Average_Sigma": 3}},

                {'name': language.GraphSetting["GraphMeanAddSubSigma"], 'type': 'int',
                 'value': GraphMeanAddSubSigma, },

                {'name': language.GraphSetting["GraphBins"], 'type': 'int',
                 'value': GraphBins,
                 'max': 100, 'min': 20},

                {'name': language.GraphSetting["GraphPlotScatterSimple"], 'type': 'bool',
                 'value': GraphPlotScatterSimple},

                {'name': language.GraphSetting["GraphPlotScatterSimpleNum"], 'type': 'int',
                 'value': GraphPlotScatterSimpleNum},

                {'name': language.GraphSetting["GraphPlotColumn"], 'type': 'int',
                 'value': GraphPlotColumn},

                {'name': language.GraphSetting["GraphPlotWidth"], 'type': 'int',
                 'value': GraphPlotWidth},

                {'name': language.GraphSetting["GraphPlotHeight"], 'type': 'int',
                 'value': GraphPlotHeight},

                {'name': language.GraphSetting["GraphCpkLoClamp"], 'type': 'float',
                 'value': GraphCpkLoClamp},

                {'name': language.GraphSetting["GraphCpkHiClamp"], 'type': 'float',
                 'value': GraphCpkHiClamp},

                {'name': language.GraphSetting["GraphTopFailClamp"], 'type': 'float',
                 'value': GraphTopFailClamp},

                {'name': language.GraphSetting["GraphRejectClamp"], 'type': 'float',
                 'value': GraphRejectClamp},

                {'name': language.GraphSetting["GraphPlotFloatRound"], 'type': 'int',
                 'value': GraphPlotFloatRound},

                {'name': language.GraphSetting["PandasScreen"], 'type': 'bool',
                 'value': PandasScreen},

                {'name': language.GraphSetting["PandasLogDefaultRow"], 'type': 'int',
                 'value': PandasLogDefaultRow},

                {'name': language.GraphSetting["HtmlReportMaxRows"], 'type': 'int',
                 'value': HtmlReportMaxRows},

            ]
        },
    ]

    PLOT_BACKEND = language.PLOT_BACKEND

    ALL_CHART_ACTIONS = [
        ACTIONS_NAME("action_distribution", "测试数据分布图"),
        ACTIONS_NAME("action_distribution_trans", "横向分布图"),
        ACTIONS_NAME("action_comparing", "比较密度图"),
        ACTIONS_NAME("action_scatter", "散点图"),
        ACTIONS_NAME("action_box_plot", "箱线图"),
        ACTIONS_NAME("action_mapping", "BIN Mapping"),
        ACTIONS_NAME("action_visual_map", "Visual Map"),
        ACTIONS_NAME("action_linear", "线性回归"),
        ACTIONS_NAME("action_bin_pareto", "Bin Pareto"),
        ACTIONS_NAME("action_multiple_chart", "多图选取"),
        ACTIONS_NAME("action_clear_jmp", "清除JMP打开的项目"),
        ACTIONS_NAME("action_contour_mapping", "等高线 Mapping"),
        ACTIONS_NAME("action_3d_scatter", "3D 散点图"),
        ACTIONS_NAME("action_calculation_table", "制程能力制表"),
    ]

    JMP_CHARTS = ALL_CHART_ACTIONS
    QT_GRAPH_CHARTS = [
        # ALL_CHART_ACTIONS[1],
        # ALL_CHART_ACTIONS[3],
        # ALL_CHART_ACTIONS[5],
        # ALL_CHART_ACTIONS[6],
    ]

    ALTAIR_CHARTS = [
        ALL_CHART_ACTIONS[3],
        ALL_CHART_ACTIONS[-1]
    ]

    R_CHARTS = [

    ]

    WEB_ACTIONS = [
        ACTIONS_NAME("action_web_search", "服务器解析数据分析"),
        ACTIONS_NAME("action_material_report", "量产测试数据报表"),
        ACTIONS_NAME("action_server_scan", "服务器数据解析状态"),
        ACTIONS_NAME("action_tools", "服务器设置工具"),
    ]

    JMP_MULTI_SELECT = ["测试数据分布图", "横向分布图", "比较密度图", "散点图", "箱线图(点型)", "箱线图(线型)", "MAPPING",
                        "VISUAL_MAP(热图)", "VISUAL_MAP(点型)", "VISUAL_MAP(等高线)", "数据Filter"]

    BASE_GROUP = [STDF_HEAD.GROUP_TEXT, STDF_HEAD.LOT_ID, STDF_HEAD.SBLOT_ID, STDF_HEAD.WAFER_ID, STDF_HEAD.FLOW_ID,
                  STDF_HEAD.TEST_COD, STDF_HEAD.SUB_CON, STDF_HEAD.NODE_NAM, STDF_HEAD.BLUE_FILM_ID, STDF_HEAD.JOB_NAM,
                  STDF_HEAD.FILE_NAME]
    DEFAULT_SELECT_BASE_GROUP = [STDF_HEAD.GROUP_TEXT, ]

    SUMMARY_GROUP = [STDF_HEAD.GROUP_TEXT, STDF_HEAD.LOT_ID, STDF_HEAD.SBLOT_ID, STDF_HEAD.WAFER_ID, STDF_HEAD.FLOW_ID,
                     STDF_HEAD.TEST_COD, STDF_HEAD.SUB_CON, STDF_HEAD.NODE_NAM, STDF_HEAD.BLUE_FILM_ID,
                     STDF_HEAD.JOB_NAM, STDF_HEAD.FILE_NAME]
    DATA_GROUP = [PRR_HEAD.HEAD_NUM, PRR_HEAD.SITE_NUM, PRR_HEAD.HARD_BIN, PRR_HEAD.SOFT_BIN]

    PROCESS_VALUE = ["MEAN", "STD", "CPK"]
    PROCESS_TOP_ITEM_LIST = ["YIELD", "DATA"]
