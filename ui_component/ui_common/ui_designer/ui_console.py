# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'console.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import pyqtsource_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1006, 540)
        self.action_select_run = QAction(MainWindow)
        self.action_select_run.setObjectName(u"action_select_run")
        icon = QIcon()
        icon.addFile(u":/pyqt/source/images/NavOverFlow_Start.png", QSize(), QIcon.Normal, QIcon.Off)
        self.action_select_run.setIcon(icon)
        self.action_run_all = QAction(MainWindow)
        self.action_run_all.setObjectName(u"action_run_all")
        icon1 = QIcon()
        icon1.addFile(u":/pyqt/source/images/icon_video.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.action_run_all.setIcon(icon1)
        self.action_refresh = QAction(MainWindow)
        self.action_refresh.setObjectName(u"action_refresh")
        icon2 = QIcon()
        icon2.addFile(u":/pyqt/source/images/lc_convertinto3d.png", QSize(), QIcon.Normal, QIcon.Off)
        self.action_refresh.setIcon(icon2)
        self.action_load_file = QAction(MainWindow)
        self.action_load_file.setObjectName(u"action_load_file")
        icon3 = QIcon()
        icon3.addFile(u":/pyqt/source/images/python.png", QSize(), QIcon.Normal, QIcon.Off)
        self.action_load_file.setIcon(icon3)
        self.action_save_to_file = QAction(MainWindow)
        self.action_save_to_file.setObjectName(u"action_save_to_file")
        icon4 = QIcon()
        icon4.addFile(u":/pyqt/source/images/Save.png", QSize(), QIcon.Normal, QIcon.Off)
        self.action_save_to_file.setIcon(icon4)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        MainWindow.setCentralWidget(self.centralwidget)
        self.toolBar = QToolBar(MainWindow)
        self.toolBar.setObjectName(u"toolBar")
        MainWindow.addToolBar(Qt.RightToolBarArea, self.toolBar)

        self.toolBar.addAction(self.action_select_run)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.action_run_all)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.action_refresh)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.action_load_file)
        self.toolBar.addAction(self.action_save_to_file)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.action_select_run.setText(QCoreApplication.translate("MainWindow", u"select_run", None))
        self.action_run_all.setText(QCoreApplication.translate("MainWindow", u"run_all", None))
        self.action_refresh.setText(QCoreApplication.translate("MainWindow", u"refresh", None))
#if QT_CONFIG(tooltip)
        self.action_refresh.setToolTip(QCoreApplication.translate("MainWindow", u"\u5237\u65b0\u6570\u636e", None))
#endif // QT_CONFIG(tooltip)
        self.action_load_file.setText(QCoreApplication.translate("MainWindow", u"load_file", None))
#if QT_CONFIG(tooltip)
        self.action_load_file.setToolTip(QCoreApplication.translate("MainWindow", u"\u52a0\u8f7d\u5e76\u8fd0\u884c\u811a\u672c\u6587\u4ef6", None))
#endif // QT_CONFIG(tooltip)
        self.action_save_to_file.setText(QCoreApplication.translate("MainWindow", u"save_to_file", None))
#if QT_CONFIG(tooltip)
        self.action_save_to_file.setToolTip(QCoreApplication.translate("MainWindow", u"\u5c06\u7f16\u8f91\u7a97\u53e3\u7684\u4ee3\u7801\u4fdd\u5b58\u5230py\u6587\u4ef6\u4e2d", None))
#endif // QT_CONFIG(tooltip)
        self.toolBar.setWindowTitle(QCoreApplication.translate("MainWindow", u"toolBar", None))
    # retranslateUi


    def retranslate(self):
        self.action_select_run.setText(QCoreApplication.translate("MainWindow", u"Select_Run", None))
        self.action_run_all.setText(QCoreApplication.translate("MainWindow", u"Run_All", None))
        self.action_refresh.setText(QCoreApplication.translate("MainWindow", u"Refresh", None))
        self.action_refresh.setToolTip(QCoreApplication.translate("MainWindow", u"Refresh Data", None))
        self.action_load_file.setText(QCoreApplication.translate("MainWindow", u"Load_File", None))
        self.action_load_file.setToolTip(QCoreApplication.translate("MainWindow", u"Load And Run The Script File", None))
        self.action_save_to_file.setText(QCoreApplication.translate("MainWindow", u"Save_To_File", None))
        self.action_save_to_file.setToolTip(QCoreApplication.translate("MainWindow", u"Save The Code Of The Editing Window To A Py File", None))
        self.toolBar.setWindowTitle(QCoreApplication.translate("MainWindow", u"Toolbar", None))
