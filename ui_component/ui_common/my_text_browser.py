#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : my_text_browser.py
@Author  : Link
@Time    : 2021/10/22 20:55
@Mark    : 
"""
import re

from PySide2.QtCore import Qt
from PySide2.QtWidgets import QTextBrowser

import datetime as dt


class Print:
    INFO = "00"
    ERROR = "01"
    WARNING = "02"
    DEBUG = "03"
    SUCCESS = "04"

    @staticmethod
    def Info(text: str):
        print("{}:==={}==={}".format(Print.INFO, dt.datetime.now().strftime("%H:%M:%S"), text))

    @staticmethod
    def Error(text: str):
        print("{}:==={}==={}".format(Print.ERROR, dt.datetime.now().strftime("%H:%M:%S"), text))

    @staticmethod
    def Warning(text: str):
        print("{}:==={}==={}".format(Print.WARNING, dt.datetime.now().strftime("%H:%M:%S"), text))

    @staticmethod
    def Debug(text: str):
        print("{}:==={}==={}".format(Print.DEBUG, dt.datetime.now().strftime("%H:%M:%S"), text))

    @staticmethod
    def Success(text: str):
        print("{}:==={}==={}".format(Print.SUCCESS, dt.datetime.now().strftime("%H:%M:%S"), text))


class UiMessage:
    code: str = "00"
    string: str = ""
    color = "#000000"

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)
        if self.code == Print.ERROR:
            self.color = "#FF0000"
            return
        if self.code == Print.WARNING:
            self.color = "#AFAF00"
            return
        if self.code == Print.DEBUG:
            self.color = "#0000FF"
            return
        if self.code == Print.SUCCESS:
            self.color = "#006400"
            return

    @staticmethod
    def Info(text: str):
        return UiMessage(code=Print.INFO, string=text)

    @staticmethod
    def Error(text: str):
        return UiMessage(code=Print.ERROR, string=text)

    @staticmethod
    def Warning(text: str):
        return UiMessage(code=Print.WARNING, string=text)

    @staticmethod
    def Debug(text: str):
        return UiMessage(code=Print.DEBUG, string=text)

    @staticmethod
    def Success(text: str):
        return UiMessage(code=Print.SUCCESS, string=text)


class MQTextBrowser(QTextBrowser):
    skip_output = {'\n'}

    def __init__(self, parent=None):
        super(MQTextBrowser, self).__init__(parent)
        # self.setTextInteractionFlags(Qt.TextEditorInteraction)
        self.setLineWrapMode(MQTextBrowser.NoWrap)
        self.setUndoRedoEnabled(True)
        self.setOpenLinks(True)
        self.setOpenExternalLinks(True)

    def split_append(self, text: str):
        if text in self.skip_output:
            return
        text_split = text.split(':', 1)
        if len(text_split) == 1:
            string = text_split[0]
            self.append(string)
            return
        code, string = text_split
        mes = UiMessage(code=code, string=string)
        self.m_append(mes)

    def m_append(self, mes: UiMessage):
        encCmd = re.sub(r'>', '&gt;', re.sub(r'<', '&lt;', mes.string))
        encCmd = re.sub(r' ', '&nbsp;', encCmd)
        encCmd = re.sub(r'\n', '<br>', encCmd)
        string = "<font size=\"4\" color=\"{}\">{}</font>".format(mes.color, encCmd)
        self.append(string)

    def mes_append(self, mes: UiMessage):
        string = "<font size=\"4\" color=\"{}\">{}</font>".format(mes.color, mes.string)
        self.append(string)
