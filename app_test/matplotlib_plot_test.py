#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : matplotlib_plot_test.py
@Author  : Link
@Time    : 2023/4/16 10:19
@Mark    : 
"""
import math
import unittest

from app_test.test_utils.log_utils import Print
from app_test.test_utils.mixins import Hdf5DataLoad
from app_test.test_utils.wrapper_utils import Tester
from common.data_class_interface.for_analysis_stdf import STDF_HEAD, PRR_HEAD
from common.li import FutureLi

import joypy
from matplotlib import pyplot as plt
import seaborn as sns


class MatplotlibPlotCase(unittest.TestCase, Hdf5DataLoad):
    """

    """
    li: FutureLi = None

    @Tester(
        ["simple_load_data"],
        exec_time=True,
    )
    def test_load_data(self):
        """
        读取数据待作图
        :return:
        """
        select_summary, id_module_dict = self.summary.load_select_data([1])
        self.li.set_data(select_summary, id_module_dict)
        self.li.concat()
        Print.print_table(self.li.df_module.ptmd_df_limit.to_dict("record"))
        self.li.set_data_group(None, ["SITE_NUM"])

        # groups = len(self.li.to_chart_csv_data.group_df)  # 使用DA_GROUP来绘制分布图
        # row = math.ceil(math.sqrt(groups))

        print(self.li.to_chart_csv_data.df.info())

    @Tester(
        ["test_load_data"],
        exec_time=True,
        skip_args_time=True,
    )
    def test_joy_plot(self):
        """
        运行时间, 用于后台数据监控
        1.4
        """
        fig, axes = joypy.joyplot(
            self.li.to_chart_csv_data.df, by=[STDF_HEAD.GROUP, PRR_HEAD.DA_GROUP], column=0,
            hist=True, bins=50, overlap=0,
            grid=True, legend=False, figsize=(6, 8),
            x_range=[-0.86, -0.80]
        )
        plt.axvline(x=-0.85, color='orangered', ls="--", label="-0.85")
        plt.axvline(x=-0.81, color='r', ls="--", label="-0.81")
        fig.show()

    @Tester(
        ["test_load_data"],
        exec_time=True,
        skip_args_time=True,
    )
    def test_group_plot(self):
        """
        0.6
        """
        groups = len(self.li.to_chart_csv_data.group_df)  # 使用DA_GROUP来绘制分布图
        row_cols = math.ceil(math.sqrt(groups))

        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        fig, axs = plt.subplots(nrows=row_cols, ncols=row_cols)
        for index, (key, df) in enumerate(self.li.to_chart_csv_data.group_df.items()):
            t_row, t_col = divmod(index, row_cols)

            axs[t_row, t_col].hist(df[0], 20, density=True, histtype='stepfilled', facecolor='g', alpha=0.75)
            axs[t_row, t_col].set(
                xlim=[-0.87, -0.75]
            )
        fig.tight_layout()
        plt.show()

    @Tester(
        ["test_load_data"],
        exec_time=True,
        skip_args_time=True,
    )
    def test_group_plot_seaborn(self):
        """
        0.619. 箱线+散点图, 不好看
        """
        # boxplot
        ax = sns.boxplot(x=PRR_HEAD.DA_GROUP, y=0, data=self.li.to_chart_csv_data.df)
        # add stripplot
        ax = sns.stripplot(x=PRR_HEAD.DA_GROUP, y=0, data=self.li.to_chart_csv_data.df, color="orange", jitter=0.2,
                           size=0.5)
        # add title
        ax.set_ylim([-0.86, -0.80])
        plt.title("Boxplot with jitter", loc="left")
        # show the graph
        plt.plot()
