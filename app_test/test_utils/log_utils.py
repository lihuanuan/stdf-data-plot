"""
-*- coding: utf-8 -*-
@Author  : Link
@Time    : 2022/11/25 15:04
@Site    : 
@File    : log_utils.py
@Software: PyCharm
@Remark  : 
"""
from typing import List

from loguru import logger
from prettytable import PrettyTable

logger_config = logger.add('logger.log', level="INFO", retention='14 days')


class Print:
    DEBUG = True

    @staticmethod
    def Info(string):
        print('\033[1;36m{}\033[0m'.format(string))

    @staticmethod
    def Debug(string):
        logger.debug(string)

    @staticmethod
    def Record(string):
        logger.info(string)

    @staticmethod
    def Success(string):
        logger.success(string)

    @staticmethod
    def Warning(string):
        logger.warning(string)

    @staticmethod
    def Danger(string):
        logger.error(string)

    @staticmethod
    def PASS():
        Print.Success("""
******************************************
******  @@@@     @    @@@@@  @@@@@  ******
******  @   @   @ @   @      @      ******
******  @@@@   @@@@@  @@@@@  @@@@@  ******
******  @      @   @      @      @  ******
******  @      @   @  @@@@@  @@@@@  ******
******************************************
        """)

    @staticmethod
    def FAIL():
        Print.Warning("""
******************************************
******  @@@@@    @      @    @      ******
******  @       @ @     @    @      ******
******  @@@@@  @@@@@    @    @      ******
******  @      @   @    @    @      ******
******  @      @   @    @    @@@@@  ******
******************************************
        """)

    @staticmethod
    def print_table(data_list: list):
        table = PrettyTable()
        # table.title = 'Calculation Ptr'
        for index, each in enumerate(data_list):
            if not isinstance(each, dict):
                each = dict(each)
            if index == 0:
                table.field_names = each.keys()
            table.add_row(each.values())
        Print.Record('\r\n' + str(table))


if __name__ == '__main__':
    Print.PASS()
