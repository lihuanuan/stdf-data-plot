#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : tcp_test.py
@Author  : Link
@Time    : 2023/2/19 10:03
@Mark    : 
"""
import unittest
import json

from app_test.test_utils.wrapper_utils import Tester
from var_language import HttpConfig
from web_core.tcp_client_interface import TcpClient, TcpClientFunc
from web_core.tcp_utils import get_local_ip


class TcpClientTestCase(unittest.TestCase):
    tcp_client: TcpClient = None

    def setUp(self) -> None:
        self.tcp_client = TcpClient(ip=get_local_ip(), port=HttpConfig.TCP_PORT)
        self.tcp_client.connect()

    def tearDown(self) -> None:
        self.tcp_client.close()

    def test_message(self):
        """
        测试丢出一个信息
        :return:
        """
        self.tcp_client.tcp_request(":MES:@TEST@DASD!\r\n", block=True)

    def test_web_load(self):
        """

        """
        t_data = json.dumps([
            {'ID': 7, 'GROUP_TYPE': 'LOT_ID|WAFER_ID', 'GROUP_TEXT': 'DEMO|', 'MIR_IDS': '23'}
        ])
        self.tcp_client.tcp_request(
            TcpClientFunc.Code(TcpClientFunc.WEB_LOAD, t_data)
        )
