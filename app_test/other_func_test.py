"""
-*- coding: utf-8 -*-
@Author  : Link
@Time    : 2022/12/26 14:48
@Site    : 
@File    : other_func_test.py
@Software: PyCharm
@Remark  : 
"""
import unittest

import pandas as pd

from app_test.test_utils.wrapper_utils import Tester


class QtUiCase(unittest.TestCase):

    @Tester()
    def test_drop_duplicate(self):
        df = pd.DataFrame([{"A": 1, "B": 1}, {"A": 1, "B": 2}, {"A": 1, "B": 1}, {"A": 1, "B": 2}, {"A": 2, "B": 2}])
        print(df[["A", "B"]].drop_duplicates())

    @Tester()
    def test_pandas_dict_data(self):
        data = {
            1: [1, 2, 3],
            2: [1, 2, 3],
            3: [1, 2, 3],
        }
        print(pd.DataFrame(data))

    @Tester()
    def test_pandas_list_data(self):
        data = [
            [1, 2, 3],
            [1, 2, 3],
            [1, 2, 3],
        ]
        print(pd.DataFrame(data))

    @Tester()
    def test_analysis_led_csv_data(self):
        """

        """
        import csv  # 生成器
        from typing import List, Dict
        keys = None
        csv_file = r"C:\Users\rkiam\Documents\WeChat Files\li865789047\FileStorage\File\2024-05\wv382e001-05（处理前）.csv"
        write_file = r"D:\wv382e001-05_PostAnalysis.csv"

        pre_csv = open(csv_file, "r", encoding="utf-8")
        post_csv = open(write_file, "w", newline="", encoding="utf-8")

        csv_writer = csv.writer(post_csv)
        csv_reader = csv.reader(pre_csv)

        for index, each in enumerate(csv_reader):
            if index == 0:
                keys = each
                continue
            if keys is None:
                raise Exception("Csv Data Not Have Key?")
            row_data: Dict[str, str] = dict(zip(keys, each))
            new_data = {
                "GLOBE": "{}{}".format(row_data["PosX"], row_data["PosY"].rjust(3, "0")),
                "CHIP_ID": "{}{}{}{}".format(
                    row_data["ReticleCol"], row_data["ReticleRow"],
                    row_data["ChipCol"].rjust(2, "0"), row_data["ChipRow"].rjust(2, "0")
                ),
                "Reticle": "{}{}".format(row_data["ReticleCol"], row_data["ReticleRow"])
            }
            write_data = {**new_data, **row_data}
            if index == 1:
                csv_writer.writerow(write_data.keys())
            csv_writer.writerow(write_data.values())
        pre_csv.close()
        post_csv.close()
