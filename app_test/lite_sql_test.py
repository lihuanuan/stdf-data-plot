"""
-*- coding: utf-8 -*-
@Author  : Link
@Time    : 2024/1/11 16:55
@Site    : 
@File    : lite_sql_test.py
@Software: PyCharm
@Remark  : 
"""
import unittest

from common.sql_interface.lite_sql import SqlLite


class TestLiteSql(unittest.TestCase):
    sql_lite: SqlLite = None

    def setUp(self) -> None:
        self.sql_lite = SqlLite()

    def tearDown(self) -> None:
        pass

    def test_heads(self):
        cache_head = [
            (),  # name,type
        ]

    def test_table_exists(self):
        print(
            self.sql_lite.table_exists("cache_table")
        )
