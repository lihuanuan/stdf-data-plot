"""
-*- coding: utf-8 -*-
@Author  : Link
@Time    : 2024/6/4 9:06
@Site    : 
@File    : stdf_convert.py
@Software: PyCharm
@Remark  : 
"""

import os
import sys
import unittest

from app_test.test_utils.log_utils import Print


class OtherCsvConvert(unittest.TestCase):

    def test_S200ConvertForOS2057(self):
        """
        适用于2.0.5.7 OS版本的CSV转STDF转换脚本
        """
        convert_path = r"Convert\S200STDFconverter2057"
        stdf_path = r"D:\SWAP\FakeStdf\do all"
        path_list = []
        for root, dirs, files in os.walk(stdf_path):
            for file in files:
                suffix = os.path.splitext(file)[-1]
                if suffix.lower() in {".csv"}:
                    path_list.append(os.path.join(root, file))
        os.chdir(convert_path)
        for s200_csv in path_list:
            os.system('STDFconverter.exe "{}"'.format(s200_csv))
