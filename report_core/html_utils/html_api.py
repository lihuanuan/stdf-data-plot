"""
-*- coding: utf-8 -*-
@Author  : Link
@Time    : 2023/9/19 14:04
@Site    : 
@File    : html_api.py
@Software: PyCharm
@Remark  : 
"""
import os

from HTMLTable import HTMLTable

from common.app_variable import GlobalVariable


class HtmlTableApi(object):

    def __init__(self, title: str, header_data: tuple, rows_data: iter):
        """
        :param title: 表单的标题
        :param header_data: 表头
        :param rows_data: 表的数据
        """
        self.title = title
        self.table = HTMLTable(caption=title, rows=())
        self.table.append_header_rows(rows=header_data)
        self.table.append_data_rows(rows=rows_data)

    def setStyle(self, align="left", cursor="pointer", color='#999990'):
        """
        可以自己添加想要的样式
        :param align: center、right、left
        :param cursor: 鼠标光标样式
        :param color: 标题颜色
        :return:
        """
        caption_style = {
            'text-align': align,
            'cursor': cursor,
            # "color": color,
            # 'background-color': '#ffffff',
            'font-size': '1.95em',
        }
        self.table.caption.set_style(style=caption_style)

    def setBorderStyle(self):
        """
        设置单元格边框、单元格样式
        :return:
        """
        border_style = {
            'border-color': '#000',
            'border-width': '1px',
            'border-style': 'solid',
            'border-collapse': 'collapse',
            # 'margin': 'auto', # 实现表格居中
        }
        # 外边框
        self.table.set_style(border_style)
        # 单元格边框
        self.table.set_cell_style(border_style)

    def setCellStyle(self):
        """
        设置单元格内容样式
        :return:
        """
        cell_style = {
            'white-space': 'nowrap',
            'text-align': 'center',
            'padding': '4px',
            'background-color': '#ffffff',
            'font-size': '0.95em',
        }
        self.table.set_cell_style(cell_style)

    def setTableHeaderStyle(self):
        """
        设置表头内容样式
        :return:
        """
        header_cell_style = {
            'text-align': 'center',
            'padding': '4px',
            'background-color': '#aae1fe',
            # 'color': '#FFFFFF',
            'font-size': '0.95em',
        }
        self.table.set_header_cell_style(header_cell_style)

    def setRowStyles(self):
        for row in self.table.iter_data_rows():
            if row[-1].value == "FAIL":
                row[-1].set_style({"background-color": "#ff1001"})
        return self

    def setAllStyle(self):
        self.setCellStyle()
        self.setTableHeaderStyle()
        self.setStyle(cursor="hand")
        self.setBorderStyle()
        return self

    def createHtml(self, file_path: str):
        """
        如果含link,则处理成为可以点击的链接
        :return:
        """
        string = self.table.to_html().replace("&lt;", "<").replace("&gt;", ">").replace("&quot;", '"')
        self.AppendWriteHtml(
            file_path=file_path,
            html_str=string
        )

    @staticmethod
    def AppendWriteHtml(file_path: str, html_str: str):
        if os.path.exists(file_path):
            bit = "a"
        else:
            bit = "w"
        with open(file_path, bit, encoding="gbk") as f:
            f.write(html_str)
